Antitaxi
========

A quick and (very!) dirty attempt to make **Tox** mobile network compatible.
It's basically a simple client/server approach in which all the calls
to tox_* APIs are sent to a server where the actual Tox instance resides.

I stole source files from: c-toxcore, dyad and others.

----------

Disclaimer
----------

Antitaxi **is not secure!!** and is not even ready for daily use.
It's an experiment to try to make toxcore usable on mobile networks where
connections are not stable and network consumption is limited.

The code is **intentionally** full of assert that can easily fail.
Both the server and the client can be probably crashed by (malicious) Tox 
friends and/or outside attackers.

I tried to keep the same logic of the Messenger object of the
[c-toxcore](https://github.com/TokTok/c-toxcore) project. It's an overkill
but makes it easier to add/remove stuff.

What should work
----------------

At the present stage it is possible to:

 - add friends
 - receive friend requests
 - delete friends
 - send standard messages and typing notifications.

Be **very** careful, antitaxi may destroy your local tox config file.
Always keep backups!

How to use it
-------------

Follow this [video](https://dl.dropboxusercontent.com/u/918497/out.webm).
If you don't manage to get it running by yourself you probably don't want to
use it.


Protocol
--------

The protocol is yet to be defined and will probably evolve over time.


TODO
----

 - Clean up the Mess!
 - Remove magic numbers
 - Remove printf
 - Document!
 - Save/send password hashes
 - Messages decay time
 - Fix Slaimer
 - Rewrite save function
 - Cleanup Tox define statements
 - Update crypto_core
 - Get rid of crypto_core and write own implementation?
 - Remove all the asserts... as soon as the code makes sense...
 - Create decent CMake files
 - Get rid of duplicated functions (endian to host, min,...)
