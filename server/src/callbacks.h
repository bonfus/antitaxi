#include <tox/tox.h>
#include "common.h"

void self_connection_status(Tox *tox, TOX_CONNECTION status, void *userData);
void callback_friend_request(Tox *tox, const uint8_t *public_key, const uint8_t *message, size_t length, void *userData);
void callback_friend_message(Tox *tox, uint32_t friend_number, TOX_MESSAGE_TYPE type, const uint8_t *message, size_t length, void *userData);
void callback_name_change(Tox *tox, uint32_t friend_number, const uint8_t *name, size_t length, void *user_data);
void callback_status_message(Tox *tox, uint32_t friend_number, const uint8_t *message, size_t length, void *user_data);
void callback_user_status(Tox *tox, uint32_t friend_number, TOX_USER_STATUS status, void *user_data);
void callback_connection_status(Tox *tox, uint32_t friend_number, TOX_CONNECTION connection_status, void *user_data);
void callback_read_receipt(Tox *tox, uint32_t fid, uint32_t receipt, void *user_data);
void callback_typing_change(Tox *tox, uint32_t fid, bool typing, void *user_data);
void incoming_file_callback_request(Tox *tox, uint32_t friend_number, uint32_t file_number, uint32_t kind, uint64_t file_size, const uint8_t *filename, size_t filename_length, void *user_data);
