#ifndef _COMMON_H
#define _COMMON_H

#include <tox/tox.h>

#include "session.h"
#include "dyad.h"
#include "logger.h"
#include "read_recpt.h"
#include "data_buf.h"


#define MAX_PASSWORD_LEN 20

typedef struct buffers_struct {
  dyad_Socket skt;
  data_buf_t data_buf;
  struct buffers_struct * next;
  struct buffers_struct * prev;
} buffers_t;

typedef struct {
  unsigned char pk[crypto_box_PUBLICKEYBYTES];
  unsigned char sk[crypto_box_SECRETKEYBYTES];
  unsigned char password[MAX_PASSWORD_LEN+1];
  uint16_t port;
  Tox *tox;
  buffers_t * buffers;
  session_t * sessions;
  read_recpt_t * receipts;
  dyad_Stream *s;
  Logger * log;
} app_t;



#endif
