#include "interpreter.h"
#include "net.h"
#include "pkt.h"
#include "utlist.h"
#include "crc.h"
#include "endian.h"

static inline size_t min(size_t a, size_t b) { return (a < b) ? a : b;}

static buffers_t * get_buffer_for_socket(app_t *a, dyad_Socket skt)
{
  buffers_t * el; // do this with compare function!!
  DL_FOREACH(a->buffers, el){
    if (el->skt == skt) {
      return el;
    }
  }
  LOGGER_DEBUG(a->log, "CREATING NEW BUFFER AT NEW DATA!! THIS SHOULD NOT HAPPEN!!" );
  buffers_t * new_buf = malloc(sizeof(buffers_t));
  new_buf->skt = skt;
  buf_reset(&(new_buf->data_buf));
  DL_APPEND(a->buffers, new_buf);
  return new_buf;
}

static void onData(dyad_Event *e) {
  
  app_t * a = (app_t*) e->udata;
  //LOGGER_DEBUG(a->log, "Received data %p %d", e->data, e->size);
  
  dyad_Socket skt = dyad_getSocket(e->stream);
  
  buffers_t * buf = get_buffer_for_socket(a, skt);
  data_buf_t * data_buf = &(buf->data_buf);
  
  size_t data_read = 0;
  while(data_read<e->size) {
  
  
    //  // packet to data
    //uint8_t * data = e->data + data_read;
    //if ( *data != THATS_MAGIC ) {
    //  LOGGER_DEBUG(a->log, "No MAGIC IN HERE!!");
    //  break;
    //}
    //
    //uint16_t _tmp = *(data+1) | (*(data+2) << 8); 
    //size_t crypt_len = sys_ltohs(_tmp);
    //LOGGER_DEBUG(a->log, "Frame reports len: %d", crypt_len);
    //if (crypt_len > MAX_CRYPTO_REQUEST_SIZE) {
    //  LOGGER_DEBUG(a->log, "Invalid len: %d", crypt_len);
    //  break;
    //}
    //
    //uint8_t crypt_crc = *(data+3);
    //LOGGER_DEBUG(a->log, "Frame crc is: %d", crypt_crc);
    //
    //uint8_t valid_crc = crc8_check(data+4, crypt_len, crypt_crc);
    //LOGGER_DEBUG(a->log, "Valid CRC is: %d", valid_crc);
    //if (valid_crc != 0) {
    //  LOGGER_DEBUG(a->log, "INVALID CRC! break!");
    //  break;
    //}
    //data_read += 4 + crypt_len;
    //
    // Public Key of current message
    unsigned char c_pk[crypto_box_PUBLICKEYBYTES];
    //
    //pkt_t pkt;
    //memcpy(pkt.crypt_data,data+4,crypt_len);
    //pkt.crypt_data_len = crypt_len;
    
    pkt_t pkt;
    
    pkt.crypt_data = NULL; //by ref! no copy! calloc(MAX_CRYPTO_REQUEST_SIZE, sizeof(uint8_t));
    
    data_read += buf_push_pull(data_buf, &pkt, data_read, e->data, e->size);
    
    
    if (pkt.crypt_data_len == 0) {
      continue;
    }
    
    msg_t * msg = pkt2msg(a->pk, a->sk, pkt, c_pk);
    
    
    
    if(msg!=NULL){
      if (msg->type != ANTITAXI_IS_IDLE_ID)
      LOGGER_DEBUG(a->log, " ===> SENDING: \n  msg->type %d\n" 
                           "  msg->sess_id %d\n"
                           "  msg->id %d\n"
                           "  msg->is_reply %d\n",
                           msg->type, msg->sess_id,
                           msg->id, msg->is_reply);
      
      session_t * sess = find_session_by_id(a->sessions, msg->sess_id);
      
      
      if (sess==NULL && msg->type != ANTITAXI_IS_ALIVE_ID) {
        // LOG ERROR exit(1)
        LOGGER_DEBUG(a->log, "Session not found!");
        buf_reset(data_buf);
        return;
      }
      // change PK if requested
      if (sess!=NULL && msg->type == ANTITAXI_IS_ALIVE_ID) {
        INIT_READ_MSG; 
        
        uint8_t * pass;
        size_t passl;
        uint16_t req_sessid;
        
        READ_UINT8STAR_REF(pass,passl,msg);
        READ_UINT16(req_sessid,msg);
        LOGGER_DEBUG(a->log, "Parsed %s len %d", pass,passl);
        
        // TODO, check asked and actual session coincide.
        
        // check password
        if (strncmp(a->password, pass, min(passl,MAX_PASSWORD_LEN)) == 0)
        {
          // define new session
          memcpy(sess->otherpk , c_pk, crypto_box_PUBLICKEYBYTES);
          LOGGER_INFO(a->log, "Public key for session %d updated!", sess->id);
        } else {
          // this is an invalid session which is not appended
          LOGGER_INFO(a->log, "Inavlid password %s. Will not reset public key for session %d.", pass, sess->id);
        }
      }
      if (sess==NULL && msg->type == ANTITAXI_IS_ALIVE_ID) {
      
        INIT_READ_MSG; 
        
        uint8_t * pass;
        size_t passl;
        uint16_t req_sessid;
        READ_UINT8STAR_REF(pass,passl,msg);
        READ_UINT16(req_sessid,msg);
        LOGGER_DEBUG(a->log, "Parsed %s len %d, req ssid: %d ", pass,passl, req_sessid);
        
        // check password
        
        if (strncmp(a->password, pass, min(passl,MAX_PASSWORD_LEN)) == 0)
        {
          // define new session
          if (req_sessid < UINT16_MAX) {
            sess = find_session_by_id(a->sessions, req_sessid);
          }
          
          if (sess!=NULL){
            memcpy(sess->otherpk , c_pk, crypto_box_PUBLICKEYBYTES);
          } else {
            sess = server_session(get_valid_id(a->sessions), 
                                  a->pk, a->sk, c_pk);
            DL_APPEND(a->sessions,sess);
            LOGGER_INFO(a->log, "Requested session ( %d ) not found. New session %d created.", req_sessid, sess->id);
          }
          LOGGER_INFO(a->log, "Public key set for session %d!", sess->id);
        } else {
          // this is an invalid session which is not appended
          sess = server_session(UINT16_MAX, a->pk, a->sk, c_pk);
          LOGGER_INFO(a->log, "Inavlid password %s.", pass);
        }
    
      }
    
      
      // this function performs action and prepares replies
      msg_t * reply = do_it(a, sess, msg);
      
      // message is no longer needed
      free_msg(msg);
      
      if (reply != NULL) {
          msg_ref_t * mref = new_message_ref(reply);
          if (reply->type == ANTITAXI_IS_ALIVE_ID)
            LL_PREPEND(sess->msgs, mref);
          else
            LL_APPEND(sess->msgs, mref);
      }
    
      // send callback messages
      send_queued_msgs_to_session(e->stream, sess);
      
      // remove temp sessions
      if (sess->id == UINT16_MAX) {
        clear_session(sess);
        free(sess);
      }
    }
  }
  
}

static void onAccept(dyad_Event *e) {
  app_t * a = (app_t*) e->udata;
  
  printf("Accepted connection\n");
  dyad_addListener(e->remote, DYAD_EVENT_DATA, onData, e->udata);
  
  //create new buf
  dyad_Socket skt = dyad_getSocket(e->remote);
  
  buffers_t * new_buf = malloc(sizeof(buffers_t));
  new_buf->skt = skt;
  buf_reset(&(new_buf->data_buf));
  DL_APPEND(a->buffers, new_buf);
  
}

static void onError(dyad_Event *e) {
  printf("server error: %s\n", e->msg);
}

void init_net(app_t * a) {
  
  a->buffers = NULL;
  
  dyad_init();

  a->s = dyad_newStream();
  dyad_addListener(a->s, DYAD_EVENT_ERROR,  onError,  a);
  dyad_addListener(a->s, DYAD_EVENT_ACCEPT, onAccept, a);
  dyad_listen(a->s, a->port);
}


void kill_net(app_t * a) {
  
  buffers_t * el, *tmp;
  DL_FOREACH_SAFE(a->buffers, el, tmp) {
    DL_DELETE(a->buffers, el);
    free(el);
  }
  
  dyad_end(a->s);
  dyad_shutdown();
}
