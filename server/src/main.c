#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <tox/tox.h>
#include <signal.h>
#include <pthread.h>


#include "dyad.h"
#include "callbacks.h"
#include "common.h"
#include "utlist.h"
#include "logger.h"
#include "net.h"


static bool signal_exit = false;
static bool is_connected = false;


static char data_filename[51];
static char keys_filename[51];


static app_t *app = NULL;



/*
 *  Tox Stuff
 */



bool save_profile(app_t *app)
{
	uint32_t save_size = tox_get_savedata_size(app->tox);
	uint8_t save_data[save_size];

	tox_get_savedata(app->tox, save_data);

	FILE *file = fopen(data_filename, "wb");
	if (file) {
		fwrite(save_data, sizeof(uint8_t), save_size, file);
		fclose(file);
		return true;
	} else {
		printf("Could not write data to disk\n");
		return false;
	}
}


bool file_exists(const char *filename)
{
	return access(filename, 0) != -1;
}

bool load_profile(app_t * app, struct Tox_Options *options)
{
	FILE *file = fopen(data_filename, "rb");

	if (file) {
		fseek(file, 0, SEEK_END);
		long file_size = ftell(file);
		fseek(file, 0, SEEK_SET);

		uint8_t *save_data = (uint8_t *)malloc(file_size * sizeof(uint8_t));
		fread(save_data, sizeof(uint8_t), file_size, file);
		fclose(file);

		options->savedata_data = save_data;
		options->savedata_type = TOX_SAVEDATA_TYPE_TOX_SAVE;
		options->savedata_length = file_size;

		TOX_ERR_NEW err;
		app->tox = tox_new(options, &err);
		free(save_data);

		return err == TOX_ERR_NEW_OK;
	}

	return false;
}


static void handle_signal(int sig)
{
	signal_exit = true;
}

static void print_log(Tox *tox, TOX_LOG_LEVEL level, const char *file, uint32_t line, const char *func,const char *message, void *user_data)
{
    printf("LOG MESSAGE: func: %s message: %s\n",func, message);
}

static void *run_tox(void *arg)
{
	Tox *tox = ((app_t *)arg)->tox;

	while(signal_exit==false) {
		tox_iterate(tox, arg);

		long long time = tox_iteration_interval(tox) * 1000000L;
		nanosleep((const struct timespec[]){{0, time}}, NULL);
	}
  pthread_exit(NULL);
	return NULL;
}



/*
 *  END Tox Stuff
 */
 
static bool save_keys(unsigned char pk[], unsigned char sk[])
{
  // crypto_box_PUBLICKEYBYTES
	FILE *file = fopen(keys_filename, "wb");
	if (file) {
		fwrite(pk, sizeof(unsigned char), crypto_box_PUBLICKEYBYTES, file);
		fwrite(sk, sizeof(unsigned char), crypto_box_PUBLICKEYBYTES, file);
		fclose(file);
		return true;
	} else {
		printf("Could not write keys to disk\n");
		return false;
	}
}

static bool load_keys(unsigned char * pk, unsigned char * sk)
{
	FILE *file = fopen(keys_filename, "rb");
	if (file) {
		fread(pk, sizeof(unsigned char), crypto_box_PUBLICKEYBYTES, file);
		fread(sk, sizeof(unsigned char), crypto_box_PUBLICKEYBYTES, file);
		fclose(file);
		return true;
	} else {
		printf("Could not read keys from disk\n");
		return false;
	}
}

static void logprint(void * app, LOGGER_LEVEL level, const char *file, int line, const char *func,
                         const char *message, void *user_data) {
    
    if (message) {
        fprintf(stderr,"antitaxi: %s\n", message);
    } else if (func) {
        fprintf(stderr,"antitaxi: %s\n", func);
    }
}


void print_pk(app_t * app) {
  int i=0;
  printf("Server Publik Key: \n");
  printf("Code friendly:\n");
  while (i<crypto_box_PUBLICKEYBYTES) {
    printf("0x%x, ",app->pk[i]);
    if ((i+1)%8==0){
      printf("\n");
    }
    i++;
  }
  printf("\n");
  
  
  char address_hex[TOX_ADDRESS_SIZE * 2 + 1];
  sodium_bin2hex(address_hex, sizeof(address_hex), app->pk, sizeof(app->pk));
  

	printf("Client friendly: %s\n", address_hex);  


	  
  
  
}

void init_app_data(app_t * app){
  
  
  //DL_APPEND(app->sessions, sess);
  app->sessions=NULL;
  app->tox=NULL;
  app->receipts=NULL;
  app->s=NULL;
  app->password[0] = '\0';
  app->port = 12345;

  
  Logger *log = logger_new();

  if (log == NULL) {
    exit(1);
  }
  app->log = log;
  
  logger_callback_log(app->log, &logprint, app, NULL);
  
  LOGGER_DEBUG(app->log, "Initialization done!");
  
}

void free_app_data(app_t * app){
  

  if (app->log)
    logger_kill(app->log);

  kill_net(app);
  
  
  session_t * sess, *sesstmp;
  DL_FOREACH_SAFE(app->sessions, sess, sesstmp) {
    clear_session(sess);
    DL_DELETE(app->sessions, sess);
    free(sess);
  }
  
  read_recpt_t * rcpt, *rcpttmp;

  DL_FOREACH_SAFE(app->receipts, rcpt, rcpttmp) {
    DL_DELETE(app->receipts, rcpt);
    free(rcpt);
  }
  
}


void gen_rand_pass(char * pass){
	int i=0;
	int x;
  unsigned long junk;
  srandom(time(NULL));
	for(i=0; i<10; i++){
		x=random();
		pass[i]=58 + (x & 0x3f);
	}
	pass[10]='\0';
}

void usage(){
  printf("ANTITAXI server.\n"
          "\n"
          "-i set host to listen\n"
          "-p port\n"
          "-k file for server keys\n"
          "-s password for server access\n"
          "-t tox data file\n"
          "-h print this help\n");
}

int main(int argc, char *argv[])
{
  
  strcpy(data_filename, "data");
  strcpy(keys_filename, "keys");
  
  signal(SIGINT, handle_signal);
  
  app=calloc(1,sizeof(app_t));
  init_app_data(app);
  
  extern char *optarg;
  extern int optind, opterr, optopt;
  int option;
  while ((option = getopt (argc, argv, "i:p:k:s:t:h?")) != -1)
  {
        switch (option)
        {
        case 'i':
            //addr = strdup(optarg);
            break;
        case 'p':
          {
            int v = strtol(optarg,NULL,10);
            if (v>0 && v < UINT16_MAX) {
              app->port = v;
            } else {
              LOGGER_ERROR(app->log, "Invalid port!");
            }
            break;
          }
        case 'k':
            strncpy(keys_filename, optarg, 50);
            keys_filename[50]='\0';
            break;
        case 's':
            strncpy(app->password, optarg, MAX_PASSWORD_LEN);
            keys_filename[MAX_PASSWORD_LEN]='\0';
            break;
        case 't':
            strncpy(data_filename, optarg, 50);
            keys_filename[50]='\0';
            break;
        default:
            usage();
            exit(0);
            break;
        }
  }
  
  
  

  // gen pk and sk
  if (!load_keys(app->pk, app->sk)) {
    LOGGER_INFO(app->log, "Generating new key pair for server.", app->password);
    crypto_box_keypair(app->pk, app->sk);
    save_keys(app->pk, app->sk);
  }
  print_pk(app);
  
  // set pass
  if (app->password[0] == '\0') {
    gen_rand_pass(app->password);
    LOGGER_INFO(app->log, "Random password generated: %s", app->password);
  }

  // init Tox
	TOX_ERR_NEW err = TOX_ERR_NEW_OK;
	struct Tox_Options options;
	tox_options_default(&options);

	if (file_exists(data_filename)) {
		if (load_profile(app, &options)) {
			printf("Loaded Tox data from disk\n");
		} else {
			printf("Failed to load Tox data from disk\n");
			return -1;
		}
	} else {
		printf("Creating a new profile\n");

		app->tox = tox_new(&options, &err);
	  const char *name = "ANTITAXI";
	  const char *status_msg = "Prends le bus Prends le bus";
    
	  tox_self_set_name(app->tox, (uint8_t *)name, strlen(name)+1, NULL);
	  tox_self_set_status_message(app->tox, (uint8_t *)status_msg, strlen(status_msg)+1, NULL);
		save_profile(app);
	}




	uint8_t address_bin[TOX_ADDRESS_SIZE];
	tox_self_get_address(app->tox, (uint8_t *)address_bin);
	char address_hex[TOX_ADDRESS_SIZE * 2 + 1];
	sodium_bin2hex(address_hex, sizeof(address_hex), address_bin, sizeof(address_bin));
	printf("%s\n", address_hex);
  
  
  
  // connect all callbacks
  tox_callback_self_connection_status(app->tox, self_connection_status);
  tox_callback_file_recv(app->tox, incoming_file_callback_request);
  //tox_callback_file_recv_control(tox, file_transfer_callback_control);
  //tox_callback_file_recv_chunk(tox, incoming_file_callback_chunk);
  //tox_callback_file_chunk_request(tox, outgoing_file_callback_chunk);
  tox_callback_friend_request(app->tox, callback_friend_request);
  tox_callback_friend_message(app->tox, callback_friend_message);
  tox_callback_friend_name(app->tox, callback_name_change);
  tox_callback_friend_status_message(app->tox, callback_status_message);
  tox_callback_friend_status(app->tox, callback_user_status);
  tox_callback_friend_typing(app->tox, callback_typing_change);
  tox_callback_friend_read_receipt(app->tox, callback_read_receipt);
  tox_callback_friend_connection_status(app->tox, callback_connection_status);
  //tox_callback_conference_invite(tox, callback_group_invite);
  //tox_callback_conference_message(tox, callback_group_message);
  //tox_callback_conference_namelist_change(tox, callback_group_namelist_change);
  //tox_callback_conference_title(tox, callback_group_topic);
  //tox_callback_friend_list_change(tox, callback_friend_list_change, NULL);
  //tox_callback_mdev_self_status_message(tox, callback_mdev_self_status_msg, NULL);
  //tox_callback_mdev_self_name(tox, callback_mdev_self_name, NULL);
  //tox_callback_mdev_self_state(tox, callback_mdev_self_state, NULL);
  //tox_callback_mdev_sent_message(tox, callback_device_sent_message, NULL);
  
  
  
  pthread_t tox_thread;
	pthread_create(&tox_thread, NULL, &run_tox, app);
  
  // end tox initialization
  
  
  init_net(app);

  while (dyad_getStreamCount() > 0 && (!signal_exit)) {
    dyad_update();
  }
  printf("Save and Quit!\n");
  pthread_join(tox_thread, NULL);
	
	save_profile(app);
	tox_kill(app->tox);
  
  free_app_data(app);

  free(app);
  return 0;
}
