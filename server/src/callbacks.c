#include "callbacks.h"
#include "session.h"
#include "msg.h"
#include "utlist.h"

#define GET_FRIEND_PUBLIC_KEY(_tox,_fid,_x)   TOX_ERR_FRIEND_GET_PUBLIC_KEY err; \
                                      if (tox_friend_get_public_key(_tox, _fid, _x, &err) == false) { \
                                        LOGGER_ERROR(a->log, "Could not get firend PK!!"); \
                                      return; \
                                    }

static void append_to_all_session(app_t * a, msg_t * msg){
  session_t * sess;
  DL_FOREACH(a->sessions, sess) {
    msg->wants_reply=false;
    msg_ref_t * msgr = new_message_ref(msg);
    msgr->send=true;
    LL_APPEND(sess->msgs, msgr);
  }
}
static bool append_to_session_id(app_t * a, msg_t * msg, uint16_t sid){
  session_t * sess;
  DL_FOREACH(a->sessions, sess) {
    if (sess->id == sid) {
      msg->wants_reply=false;
      msg_ref_t * msgr = new_message_ref(msg);
      msgr->send=true;
      LL_APPEND(sess->msgs, msgr);
      return true;
    }
  }
  return false;
}

void self_connection_status(Tox *tox, TOX_CONNECTION status, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "self_connection_status callback, new value: %d", status);
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_SELF_CONNECTION_STATUS_CB_ID, false,
                                    PRTCL_UINT8, NO_MORE_FIELDS);
  
  
  INIT_WRITE_MSG
  WRITE_UINT8((uint8_t) status, msg);
  
  append_to_all_session(a,msg);
  
}

void callback_friend_request(Tox *tox, const uint8_t *public_key, const uint8_t *message, size_t length, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_friend_request callback");
  
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_FRIEND_REQUEST_CB_ID, false,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8STAR, length,
                                    NO_MORE_FIELDS);
  
  
  INIT_WRITE_MSG
  WRITE_UINT8STAR(public_key, TOX_PUBLIC_KEY_SIZE, msg);
  WRITE_UINT8STAR(message, length, msg);
  
  append_to_all_session(a,msg);
  
}

void callback_friend_message(Tox *tox, uint32_t friend_number, TOX_MESSAGE_TYPE type, const uint8_t *message,
                              size_t length, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_friend_message callback");

  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,friend_number,pk);
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_FRIEND_MESSAGE_CB_ID, false,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8,
                                    PRTCL_UINT8STAR, length,
                                    NO_MORE_FIELDS);
  
  INIT_WRITE_MSG
  WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
  WRITE_UINT8((uint8_t) type, msg);
  WRITE_UINT8STAR(message, length, msg);
  
  append_to_all_session(a, msg);
  
}

void callback_name_change(Tox *tox, uint32_t friend_number, const uint8_t *name, size_t length, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_friend_message callback");

  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,friend_number,pk);
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_FRIEND_NAME_CB_ID, false,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8STAR, length,
                                    NO_MORE_FIELDS);
  
  
  INIT_WRITE_MSG
  WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
  WRITE_UINT8STAR(name, length, msg);
  
  append_to_all_session(a,msg);
  
}

void callback_status_message(Tox *tox, uint32_t friend_number, const uint8_t *message, size_t length, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_friend_message callback");

  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,friend_number,pk);
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_FRIEND_STATUS_MESSAGE_CB_ID, false,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8STAR, length,
                                    NO_MORE_FIELDS);
  
  
  INIT_WRITE_MSG
  WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
  WRITE_UINT8STAR(message, length, msg);
  
  append_to_all_session(a,msg);
  
}

void callback_user_status(Tox *tox, uint32_t friend_number, TOX_USER_STATUS status, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_friend_message callback");

  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,friend_number,pk);
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_FRIEND_STATUS_CB_ID, false,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8,
                                    NO_MORE_FIELDS);
  
  
  INIT_WRITE_MSG
  WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
  uint8_t val = (uint8_t) status;
  WRITE_UINT8(val, msg);
  
  append_to_all_session(a,msg);
  
}

void callback_connection_status(Tox *tox, uint32_t friend_number, TOX_CONNECTION connection_status, void *user_data)
{
  
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_friend_message callback");

  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,friend_number,pk);
  
  // server messages never need a reply
  msg_t * msg = new_empty_message(ATTOX_FRIEND_CONNECTION_STATUS_CB_ID, false,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8,
                                    NO_MORE_FIELDS);
  
  
  INIT_WRITE_MSG
  WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
  uint8_t val = (uint8_t) connection_status;
  WRITE_UINT8(val, msg);
  
  append_to_all_session(a,msg);
  
}

void callback_read_receipt(Tox *tox, uint32_t fid, uint32_t receipt, void *user_data)
{
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_read_receipt callback");
  
  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,fid,pk);
  
  read_recpt_t * el, *tmp;
  DL_FOREACH_SAFE(a->receipts, el, tmp) {
    if (el->friend_id == fid && el->tox_msg_id == receipt)
    {
      
      // server messages never need a reply
      msg_t * msg = new_empty_message(ATTOX_FRIEND_READ_RECEIPT_CB_ID, false,
                                        PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                        PRTCL_UINT32,
                                        NO_MORE_FIELDS);
      
      
      INIT_WRITE_MSG
      WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
      uint32_t atid = el->antitaxi_msg_id;
      WRITE_UINT32(atid, msg);
      
      append_to_session_id(a, msg, el->session_id);
      DL_DELETE(a->receipts, el);
      free(el);
      
      return;
    }
  }
  LOGGER_DEBUG(a->log, "Receipt not found!");
}

void callback_typing_change(Tox *tox, uint32_t fid, bool typing, void *user_data) {
  app_t * a = (app_t*) user_data;
  LOGGER_DEBUG(a->log, "In callback_typing_change callback");
  

  uint8_t pk[TOX_PUBLIC_KEY_SIZE];
  GET_FRIEND_PUBLIC_KEY(tox,fid,pk);
    
  
  // server messages never need a reply                        V
  msg_t * msg = new_empty_message(ATTOX_FRIEND_TYPING_CB_ID, false,
                                  PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                  PRTCL_BOOL,
                                  NO_MORE_FIELDS);
  INIT_WRITE_MSG;
  WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
  WRITE_BOOL(typing, msg);
  append_to_all_session(a,msg);
  
}


void incoming_file_callback_request(Tox *tox, uint32_t friend_number, uint32_t file_number, uint32_t kind, uint64_t file_size, const uint8_t *filename, size_t filename_length, void *user_data)
{
	if (kind == TOX_FILE_KIND_AVATAR) {
		return;
	}

	tox_file_control(tox, friend_number, file_number, TOX_FILE_CONTROL_CANCEL, NULL);

	const char *msg = "Sorry, I don't support file transfers (yet).";
	tox_friend_send_message(tox, friend_number, TOX_MESSAGE_TYPE_NORMAL, (uint8_t*)msg, strlen(msg), NULL);
}


// Callback for toxAV...maybe in the future...
//void call(ToxAV *toxAV, uint32_t friend_number, bool audio_enabled, bool video_enabled, void *user_data)
//{
//	if (kind == TOX_FILE_KIND_AVATAR) {
//		return;
//	}
//
//	toxav_call_control(toxAV, friend_number, TOXAV_CALL_CONTROL_CANCEL, NULL);
//
//	const char *msg = "Sorry, I don't support calls (yet).";
//	tox_friend_send_message(tox, friend_number, TOX_MESSAGE_TYPE_NORMAL, (uint8_t*)msg, strlen(msg), NULL);
//}
