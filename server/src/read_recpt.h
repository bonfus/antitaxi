#include <inttypes.h>

typedef struct read_recpt_struct {
  uint32_t tox_msg_id;
  uint32_t friend_id;
  uint32_t antitaxi_msg_id;
  uint16_t session_id;
  struct read_recpt_struct * prev;
  struct read_recpt_struct * next;
} read_recpt_t;
