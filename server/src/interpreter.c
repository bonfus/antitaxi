#include <math.h>    // for ceil
#include <assert.h>
#include <tox/tox.h>


#include "protocol.h"
#include "msg.h"
#include "common.h"
#include "utlist.h"
#include "protocol.h"

static inline size_t min(size_t a, size_t b) { return (a < b) ? a : b;}

static uint32_t msg2fnum(size_t rd, uint32_t * fnum, msg_t * msg, const app_t * a) {
  uint8_t * pk;
  size_t pklen;
  TOX_ERR_FRIEND_BY_PUBLIC_KEY pkerr;
  size_t add = 0; //INIT_READ_MSG;
  READ_UINT8STAR_REF(pk,pklen,msg); //rd is incremented here
  if (pklen != TOX_PUBLIC_KEY_SIZE) {
    *fnum = UINT32_MAX; 
    return rd;
  }
  *fnum = tox_friend_by_public_key(a->tox, pk, &pkerr);
  if (pkerr != TOX_ERR_FRIEND_BY_PUBLIC_KEY_OK)
    *fnum = UINT32_MAX;
  
  return rd;
}

msg_t * do_it(app_t * a, session_t* sess, msg_t * msg)
{
  // check message age
  
  //do the action!
  switch(msg->type) { 
    case ANTITAXI_IS_ALIVE_ID:
    {
      LOGGER_DEBUG(a->log, "Replying a ANTITAXI_IS_ALIVE_ID");
      sess->bootstrap_done=false;
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, PRTCL_UINT16, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG(reply);
      
      if (sess->id == UINT16_MAX) {
        WRITE_UINT8(1, reply); 
      } else {
        WRITE_UINT8(0, reply);
      }
      WRITE_UINT16(sess->id, reply);

      return reply; 
    }
    case ANTITAXI_IS_IDLE_ID:
    {
      sess->bootstrap_done = true;
      //LOGGER_DEBUG(a->log, "Received a ANTITAXI_IS_IDLE_ID");
      return NULL; 
    }
    case ATTOX_SELF_GET_CONNECTION_STATUS_ID:
    {
      TOX_CONNECTION ret = tox_self_get_connection_status(a->tox);
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8,
                                                NO_MORE_FIELDS);
      INIT_WRITE_MSG(reply);
      uint8_t uret = (uint8_t) ret;
      WRITE_UINT8(uret, reply);
      return reply;
    }
    case ANTITAXI_GET_SAVEDATA_ID:
    {
      LOGGER_DEBUG(a->log, "Received a ANTITAXI_GET_SAVEDATA_ID");
      
      size_t curr_data_pos_s, chunk_size;
      
      INIT_READ_MSG; 
      READ_SIZET(curr_data_pos_s,msg);
      
      LOGGER_DEBUG(a->log, "Parsed pos %d", curr_data_pos_s);
      int datas = (int) tox_get_savedata_size(a->tox);
      int curr_data_pos = (int)curr_data_pos_s;
      
      uint8_t * data = malloc(datas*sizeof(uint8_t));
      tox_get_savedata(a->tox, data);
      
      if (curr_data_pos < datas) {
        LOGGER_DEBUG(a->log, "curr_data_pos %d,  datas  %d", curr_data_pos , datas);
        chunk_size = min(datas-curr_data_pos, 
                          (MAX_ANTITAXI_DATA_REQUEST_SIZE)-20); //  5 + 5 + 5 + 5
        
        LOGGER_DEBUG(a->log, "chunk_size %d", chunk_size);                  
        msg_t * reply = new_reply_message(msg, PRTCL_SIZET, PRTCL_SIZET, 
                                                PRTCL_SIZET, PRTCL_UINT8STAR, 
                                                chunk_size,
                                                NO_MORE_FIELDS);
        INIT_WRITE_MSG(reply);
        WRITE_SIZET(datas, reply);         // 5
        WRITE_SIZET(curr_data_pos, reply); // 5
        WRITE_SIZET(chunk_size, reply);    // 5
        uint8_t * from = data+curr_data_pos; // header is 5
        WRITE_UINT8STAR(from, chunk_size, reply);
        free(data);
        return reply;
      }
      free(data);
      return NULL;
    }
    
    case ATTOX_SELF_SET_NAME_ID:
    {
        
      LOGGER_DEBUG(a->log, "Received a TOX_SELF_SET_NAME");
      
      TOX_ERR_SET_INFO err;
      uint8_t * name;
      size_t namel;
      
      INIT_READ_MSG; 
      READ_UINT8STAR_REF(name,namel,msg); 
      LOGGER_DEBUG(a->log, "Parsed \"%s\" len %d rd %d", name, namel, rd);
      
      tox_self_set_name(a->tox, name, namel, &err);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply); 

      return reply;
    }
    
    case ATTOX_SELF_GET_PUBLIC_KEY_ID:
    {
        
      LOGGER_DEBUG(a->log, "Received a ATTOX_SELF_GET_PUBLIC_KEY_ID");
      
      uint8_t pk[TOX_PUBLIC_KEY_SIZE];
      tox_self_get_public_key(a->tox, pk);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE , reply);

      return reply; 
    }
    
    case ATTOX_SELF_GET_NOSPAM_ID:
    {
        
      LOGGER_DEBUG(a->log, "Received a ATTOX_SELF_GET_NOSPAM_ID");
      
      uint32_t nspm = tox_self_get_nospam(a->tox);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT32, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT32(nspm , reply);

      return reply; 
    }
    
    case ATTOX_SELF_GET_ADDRESS_ID:
    {
        
      LOGGER_DEBUG(a->log, "Received a ATTOX_SELF_GET_ADDRESS_ID");
      
      uint8_t addr[TOX_ADDRESS_SIZE];
      
      
      tox_self_get_address(a->tox, addr);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8STAR, TOX_ADDRESS_SIZE,  NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8STAR(addr, TOX_ADDRESS_SIZE , reply);

      return reply; 
    }
    
    case ATTOX_SELF_SET_STATUS_MESSAGE_ID:
    {
        
      LOGGER_DEBUG(a->log, "Received a ATTOX_SELF_SET_STATUS_MESSAGE_ID");
      
      TOX_ERR_SET_INFO err;
      uint8_t * status;
      size_t statusl;
      
      INIT_READ_MSG; 
      READ_UINT8STAR_REF(status,statusl,msg); 
      LOGGER_DEBUG(a->log, "Parsed \"%s\" len %d", status, statusl);
      
      tox_self_set_status_message(a->tox, status, statusl, &err);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply); 

      return reply; 
    }
    
    case ATTOX_SELF_GET_STATUS_MESSAGE_ID:
    {
        
      LOGGER_DEBUG(a->log, "Received a ATTOX_SELF_GET_STATUS_MESSAGE_ID");
      
      size_t statusl=tox_self_get_status_message_size(a->tox);
      uint8_t * status = calloc(statusl,sizeof(uint8_t));
      tox_self_get_status_message(a->tox, status);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8STAR, statusl, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8STAR(status, statusl, reply); 
      free(status);
      
      return reply;
    }
    case ATTOX_FRIEND_ADD_ID:
    {
      LOGGER_DEBUG(a->log, "Received a ATTOX_FRIEND_ADD_ID");
      
      
      uint8_t * address;
      size_t addressl;
      uint8_t * reqmsg;
      size_t reqmsgl;
      
      INIT_READ_MSG; 
      READ_UINT8STAR_REF(address,addressl,msg); 
      if (addressl != TOX_ADDRESS_SIZE) {
        LOGGER_DEBUG(a->log, "Invalid address");
        
      }
      
      
      READ_UINT8STAR_REF(reqmsg,reqmsgl,msg); 
      LOGGER_DEBUG(a->log, "Parsed \"%s\" len %d", reqmsg, reqmsgl);
      
      TOX_ERR_FRIEND_ADD err;
      tox_friend_add(a->tox, address, reqmsg, reqmsgl, &err);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply);
      
      return reply;
      
    }
    case ATTOX_FRIEND_ADD_NOREQUEST_ID:
    {
      LOGGER_DEBUG(a->log, "Received a ATTOX_FRIEND_ADD_NOREQUEST_ID");
      
      
      uint8_t * pk;
      size_t pkl;
      
      INIT_READ_MSG; 
      READ_UINT8STAR_REF(pk,pkl,msg); 
      if (pkl != TOX_PUBLIC_KEY_SIZE) {
        LOGGER_DEBUG(a->log, "Invalid public key");
        
      }

      TOX_ERR_FRIEND_ADD err;
      tox_friend_add_norequest(a->tox, pk, &err);

      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply);
      
      return reply;
      
    }
    case ATTOX_FRIEND_SEND_MESSAGE_ID:
    {
      LOGGER_DEBUG(a->log, "Received a ATTOX_FRIEND_SEND_MESSAGE_ID");
      
      
      uint32_t fnum;
      uint8_t type;
      uint8_t * mesg;
      size_t mesgl;
      
      INIT_READ_MSG; 
      rd = msg2fnum(rd,&fnum,msg,a);
      READ_UINT8(type,msg); 
      READ_UINT8STAR_REF(mesg,mesgl,msg); 

      TOX_ERR_FRIEND_SEND_MESSAGE err;
      uint32_t tox_msg_id = tox_friend_send_message(a->tox, fnum, type, mesg, mesgl, &err);
      
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, PRTCL_BOOL, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply);
      bool is_keeping = false;
      WRITE_BOOL(is_keeping, reply); // is KEEPING!! TODO, IMPLEMENT THIS
      
      read_recpt_t * rec = malloc(sizeof(read_recpt_t));
      rec->friend_id = fnum;
      rec->tox_msg_id = tox_msg_id;
      rec->session_id = msg->sess_id;
      rec->antitaxi_msg_id = msg->id;
      DL_APPEND(a->receipts, rec);
      
      return reply;
    }
    case ATTOX_FRIEND_GET_CONNECTION_STATUS_ID:
    {
      
      
      
      uint32_t fnum;
      
      INIT_READ_MSG; 
      rd = msg2fnum(rd,&fnum,msg,a);

      LOGGER_DEBUG(a->log, "Received a ATTOX_FRIEND_GET_CONNECTION_STATUS_ID for friend %d.", fnum);

      TOX_CONNECTION val;
      TOX_ERR_FRIEND_QUERY  err;
      val = tox_friend_get_connection_status(a->tox, fnum, &err);

      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, PRTCL_UINT8, NO_MORE_FIELDS);
      
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) val, reply);
      WRITE_UINT8((uint8_t) err, reply);
      
      return reply;
    }
    case ATTOX_SELF_SET_TYPING_ID:
    {
      uint32_t fnum;
      bool typing;
      TOX_ERR_SET_TYPING err = TOX_ERR_SET_TYPING_FRIEND_NOT_FOUND;
      
      INIT_READ_MSG;               // rd is defined here
      rd = msg2fnum(rd,&fnum,msg,a); // increment rd here
      READ_BOOL(typing, msg);
      if (fnum < UINT32_MAX) {
        tox_self_set_typing(a->tox, fnum, typing, &err);
      }
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, NO_MORE_FIELDS);
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply);
      return reply;
    }
    case ATTOX_FRIEND_DELETE_ID:
    {
      uint32_t fnum;
      bool typing;
      TOX_ERR_FRIEND_DELETE err = TOX_ERR_FRIEND_DELETE_FRIEND_NOT_FOUND;
      
      INIT_READ_MSG;               // rd is defined here
      rd = msg2fnum(rd,&fnum,msg,a); // increment rd here
     
      if (fnum < UINT32_MAX) {
        tox_friend_delete(a->tox, fnum, &err);
      }
      // create reply
      msg_t * reply = new_reply_message(msg, PRTCL_UINT8, NO_MORE_FIELDS);
      INIT_WRITE_MSG;
      WRITE_UINT8((uint8_t) err, reply);
      return reply;
    }
    
    
    default:
    {
      LOGGER_DEBUG(a->log, "Received unimplemented message: %d", msg->type);
      return NULL;
    }
  }
  
}
