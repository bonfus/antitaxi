#ifndef _NET_H
#define _NET_H
#include "common.h"
#include "dyad.h"
#include "msg_sender.h"

void init_net(app_t * a);
void kill_net(app_t * a);

#endif
