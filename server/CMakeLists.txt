
cmake_minimum_required (VERSION 2.6)



project ("antitaxi")
set (PROJECT_VERSION_MAJOR 0)
set (PROJECT_VERSION_MINOR 1)
set (PROJECT_VERSION_PATCH 0)

set (CMAKE_BUILD_TYPE Debug) # Debug, RelWithDebInfo, Release.
set (CMAKE_VERBOSE_MAKEFILE TRUE)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(ModulePackage)

pkg_use_module(LIBSODIUM  REQUIRED  libsodium     )
pkg_use_module(TOXCORE    REQUIRED  libtoxcore    )

find_package(Threads   REQUIRED)

add_definitions(-D_SERVER)


#option(DEBUG "Enable assertions and other debugging facilities" OFF)
#if(DEBUG)
  set(MIN_LOGGER_LEVEL DEBUG)
#  add_definitions(-DTOX_DEBUG=1)
#  check_c_compiler_flag("-g3" HAVE_G3)
#  if(HAVE_G3)
#    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g3")
#  endif()
#endif()
#
#option(TRACE "Enable TRACE level logging (expensive, for network debugging)" OFF)
#if(TRACE)
#  set(MIN_LOGGER_LEVEL TRACE)
#endif()
#
if(MIN_LOGGER_LEVEL)
  add_definitions(-DMIN_LOGGER_LEVEL=LOG_${MIN_LOGGER_LEVEL})
endif()

# Main binary.
set (binary_name "${PROJECT_NAME}")
add_executable ("${binary_name}"
                src/main.c
                src/data_buf.c
                src/logger.c
                src/crc.c
                src/crypto_core.c
                src/pkt.c
                src/callbacks.c
                src/endian.c
                src/msg_sender.c
                src/session.c
                src/dyad.c
                src/interpreter.c
                src/msg.c
                src/net.c
               )

target_link_modules(antitaxi ${LIBSODIUM_LIBRARIES})
target_link_modules(antitaxi ${TOXCORE_LIBRARIES})

# Compilation flags.
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror=implicit-function-declaration -Wstrict-overflow=5")
set (CMAKE_CXX_FLAGS_DEBUG "-O0 -g")
#set (CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")
#set (CMAKE_CXX_FLAGS_RELEASE "-O3")
