#include <inttypes.h>
#include "pkt.h"
#include "msg_sender.h"
#include "utlist.h"
#include "crc.h"
#include "protocol.h"
#include "endian.h"
//void send_queued_msgs_to_all_sessions(dyad_Stream * s, session_t *sessions_head)
//{
//  session_t *sess, *stmp;
//  
//  DL_FOREACH_SAFE(sessions_head, sess, stmp) {
//    msg_ref_t * msgr, *msgrtmp;
//    LL_FOREACH_SAFE(sess->msgs, msgr, msgrtmp) {
//      
//      // message already sent
//      if (msgr->send==false) {
//        continue;
//      }
//      
//      msg_t * msg = msgr->msg;
//      // prepare msg
//      write_msg_header(sess->id,msg);
//      
//      dyad_write(s, msg->data, msg->data_len);
//      
//      msgr->send = false;
//      
//      if (msg->wants_reply == false && msgr->send == false) {
//        rem_message_ref(msgr);
//        LL_DELETE(sess->msgs, msgr);
//        free(msgr);
//      }
//      
//    }
//  }
//}

void send_queued_msgs_to_session(dyad_Stream * s, session_t *session)
{

  if (dyad_getState(s) != DYAD_STATE_CONNECTED) {
    //printf("DYAD is not CONNECTED!\n");
    return;
  }
  msg_ref_t * msgr, *msgrtmp;
  LL_FOREACH_SAFE(session->msgs, msgr, msgrtmp) {
    
    // message already sent or is reply
    if (msgr->send==false) {
      continue;
    }
    
    
    msg_t * msg = msgr->msg;
    assert(msg != NULL);
    
    
    
    // don't send messages if not authenticated
    if((session->id == UINT16_MAX) && (msg->type != ANTITAXI_IS_ALIVE_ID)) {
      printf("Not sending msg type: %d. Reason: not authenticated\n", msg->type);
      continue;
    }
    // don't send messages before bootstrap
    if(session->bootstrap_done == false)
      if ((msg->type != ANTITAXI_IS_ALIVE_ID) && 
            (msg->type != ANTITAXI_GET_SAVEDATA_ID) &&
            (msg->type != ANTITAXI_IS_IDLE_ID) ) {
      printf("Not sending msg type: %d. Reason: not boostrapped\n", msg->type);
      continue;
    }
    
    
    //printf("SENDING msg type: %d\n", msg->type);
    // prepare msg
    //write_msg_header(session->id,msg);
    //dyad_write(s, msg->data, msg->data_len);
    
    pkt_t pkt;

    pkt.crypt_data = calloc(MAX_CRYPTO_REQUEST_SIZE, sizeof(uint8_t));
    
    assert(pkt.crypt_data != NULL);
    
    msg2pkt(session, msg, &pkt);
    
    assert(pkt.crypt_data_len > 0);
    
    // clean up this! TODO
    uint8_t this_is_magic = THATS_MAGIC;
    dyad_write(s, &this_is_magic, sizeof(uint8_t));
    dyad_write(s, &this_is_magic, sizeof(uint8_t));
    dyad_write(s, &this_is_magic, sizeof(uint8_t));
    dyad_write(s, &this_is_magic, sizeof(uint8_t));
    uint16_t _l = sys_htols(pkt.crypt_data_len);
    dyad_write(s, &_l , sizeof(uint16_t));
    uint8_t crcval = crc8_calc(pkt.crypt_data, pkt.crypt_data_len);
    dyad_write(s, &crcval, sizeof(uint8_t));
    dyad_write(s, pkt.crypt_data, pkt.crypt_data_len);
    
    
    free(pkt.crypt_data);
    
    msgr->send = false;
    
    if (msg->wants_reply == false && msgr->send == false) {
      rem_message_ref(msgr);
      LL_DELETE(session->msgs, msgr);
      free(msgr);
    }
  }
}


//void send_reply_msg_to_session(dyad_Stream * s, msg_t* msg, uint16_t session_id) {
//
//  // sanity checks
//  
//  if (msg == NULL){
//    //TODO warn error exit 0
//    return;
//  }
//  
//  if (msg->is_reply == false) {
//    //TODO warn error exit 0
//    return;
//  }
//  
//  // prepare msg
//  write_msg_header(session_id,msg);
//  
//  // encrypt message
//  // TODO
//  // send msg
//  dyad_write(s, msg->data, msg->data_len);
//    
//}
