
/* sessions */

#ifndef _SESSION_H
#define _SESSION_H

#include "msg.h"
#include "crypto_core.h"

typedef struct msg_ref_struct {
  msg_t * msg;
  bool send;     // false if already sent or is a callback
  struct msg_ref_struct * next;
} msg_ref_t;

typedef struct session_struct {
  msg_ref_t * msgs;
  uint16_t id;
  bool bootstrap_done;
  unsigned char otherpk[crypto_box_PUBLICKEYBYTES];
  unsigned char mypk[crypto_box_PUBLICKEYBYTES];
  unsigned char mysk[crypto_box_SECRETKEYBYTES];
  struct session_struct * next;
  struct session_struct * prev;
} session_t;

session_t * client_session(uint16_t id, unsigned char otherpk[]);
session_t * server_session(uint16_t id, unsigned char pk[], unsigned char sk[], unsigned char cpk[]);
void clear_session(session_t * sess);
uint16_t get_valid_id(session_t * sess_head);
//session_t * add_session(uint16_t id, unsigned char pk[], unsigned char sk[]);
session_t * find_session_by_id(session_t *sessions_head, uint16_t id);
msg_ref_t * new_message_ref(msg_t * msg);
msg_ref_t * new_callback_ref(msg_t * msg);
msg_ref_t * rem_message_ref(msg_ref_t * msgr);
int count_unsent_messages(session_t * sess);
int count_unprocessed_messages(session_t * sess);

#endif
/* end sessions */
