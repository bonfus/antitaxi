#include <stdbool.h>
#include "interpreter.h"

static uint32_t msg2fnum(size_t rd, uint32_t * fnum, msg_t * msg, const Mess * m) {
  uint8_t * pk;
  size_t pklen;
  TOX_ERR_FRIEND_BY_PUBLIC_KEY pkerr;
  size_t add = 0; //INIT_READ_MSG;
  READ_UINT8STAR_REF(pk,pklen,msg); //rd is incremented here
  if (pklen != TOX_PUBLIC_KEY_SIZE) {
    *fnum = UINT32_MAX; 
    return rd;
  }
  int32_t fn = getfriend_id(m, pk);
  if (fn >0)
    *fnum = (uint32_t) fn;
  else
    *fnum = UINT32_MAX;
  
  return rd;
}

void deal_with_messages(Mess * m, void * userdata) {
  msg_ref_t * mref, *mreftmp;
  
  //int c;
  //LL_COUNT(m->net->sessions->msgs, mreftmp, c);
  //LOGGER_DEBUG(m->log, "There are %d messages to do...", c);
  
  //first session only
  LL_FOREACH_SAFE(m->net->sessions->msgs, mref, mreftmp){
    msg_t * msg = mref->msg;
    assert(msg!=NULL);
    if (do_it(m, msg, userdata)){
      rem_message_ref(mref); // this will clean the message if needed
      LL_DELETE(m->net->sessions->msgs, mref);
      free(mref);
    }
  }
}

bool do_it(Mess * m, msg_t * msg, void * userdata)
{
  // check message age
  
  //do the action!
  msg_t * reply = msg->reply;
  switch(msg->type) {
    case ANTITAXI_IS_ALIVE_ID:
    {
      if (reply) {
        
        uint8_t response;
        uint16_t sid;
        INIT_READ_MSG;
        READ_UINT8(response, reply);
        READ_UINT16(sid, reply);
        // auth succeded!
        if (response == 0) {
          LOGGER_DEBUG(m->log, "AUTHENTICATED!");
          m->net->sessions->id = sid;
          m->net->last_session_id = sid;
        } else {
          LOGGER_DEBUG(m->log, "AUTHENTICATION FAILED!");
        }
        
        return true;
      }
      return false;
    }
    case ANTITAXI_GET_SAVEDATA_ID:
    {
      if (reply) {
        LOGGER_DEBUG(m->log, "ANTITAXI_GET_SAVEDATA_ID reply.");
        size_t savedata_len, chunk_pos, chunk_len;
        
        INIT_READ_MSG;
        READ_SIZET(savedata_len, reply);
        if ((m->savedata).data_length == 0)
        {
          (m->savedata).data_length = savedata_len;
          if ((m->savedata).data != NULL || (m->savedata).data_received) {
            LOGGER_ERROR(m->log, "This should not happen!");
            (m->savedata).data = realloc((m->savedata).data, savedata_len*sizeof(uint8_t));
            (m->savedata).data_received = realloc((m->savedata).data_received, savedata_len*sizeof(bool));
          }
          (m->savedata).data = calloc(savedata_len, sizeof(uint8_t));
          (m->savedata).data_received = calloc(savedata_len, sizeof(bool));
          memset((m->savedata).data_received, false, savedata_len);
        }
        
        assert((m->savedata).data_received!=NULL);
        assert((m->savedata).data!=NULL);
        
        READ_SIZET(chunk_pos, reply);
        
        if (chunk_pos > savedata_len) {
          LOGGER_ERROR(m->log, "Invalid data position!");
        }
        READ_SIZET(chunk_len, reply);
        if (chunk_pos + chunk_len> savedata_len) {
          LOGGER_ERROR(m->log, "Invalid data position! ");
          return true;
        }
        uint8_t * poss= (m->savedata.data)+chunk_pos;
        
        READ_UINT8STAR_CPY(poss, chunk_len, reply);
        
        
        if ((chunk_pos + chunk_len) > savedata_len) {
          LOGGER_ERROR(m->log, "Invalid data position! The program will crash");
        }
        LOGGER_DEBUG(m->log, "ANTITAXI_GET_SAVEDATA_ID reply, chunk_pos %d , chunk_len %d",chunk_pos, chunk_len);
        bool * bposs = (m->savedata).data_received + chunk_pos;
        memset(bposs, true, chunk_len);
        
        return true;
      }
      return false;
    }
    case ATTOX_SELF_SET_NAME_ID:
    {
      if (msg->reply) {
        TOX_ERR_SET_INFO err;

        uint8_t u8err;
        
        INIT_READ_MSG;
        READ_UINT8(u8err,msg->reply);

        err = (TOX_ERR_SET_INFO) u8err;
        
        if (err != TOX_ERR_SET_INFO_OK) {
          //???
        }
        
        return true; // message has been processed and can be removed
        
      }
      return false;
    }
    case ATTOX_SELF_GET_PUBLIC_KEY_ID:
    {
      if (msg->reply) {
        
        uint8_t * pk;
        size_t pksize;
        
        INIT_READ_MSG;
        READ_UINT8STAR_REF(pk, pksize ,msg->reply);
        
        if (pksize == TOX_PUBLIC_KEY_SIZE) {
          LOGGER_DEBUG(m->log, "Setting publik key.");
          set_public_key(m, pk);
        } else {
          LOGGER_DEBUG(m->log, "Invalid Public key received!");
        }
        
        return true;
      }
      return false; break;
    }
    
    case ATTOX_SELF_GET_NOSPAM_ID:
    {
      if (msg->reply) {
        
        uint32_t nspm;
        
        INIT_READ_MSG;
        READ_UINT32(nspm,msg->reply);
        set_nospam(&(m->fr), nspm);
        
        return true;
      }
      return false; break;
    }
    
    case ATTOX_SELF_GET_STATUS_MESSAGE_ID:
    {
      if (msg->reply) {
        uint8_t * status;
        size_t statusl;
        INIT_READ_MSG;
        READ_UINT8STAR_REF(status,statusl,msg->reply);
        m_set_statusmessage(m, status,statusl);
        return true;
      }
      return false; break;
    }
    case ATTOX_SELF_SET_STATUS_MESSAGE_ID:
    {
      if (msg->reply) {
        TOX_ERR_SET_INFO err;

        uint8_t u8err;
        
        INIT_READ_MSG;
        READ_UINT8(u8err,msg->reply);

        err = (TOX_ERR_SET_INFO) u8err;
        
        if (err != TOX_ERR_SET_INFO_OK) {
          //???
        }
        return true;
      }
      return false; break;
    }
    case ATTOX_FRIEND_GET_CONNECTION_STATUS_ID:
    {
      uint32_t fnum;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      if (msg->reply) {
        uint8_t cstat, err;
        INIT_READ_MSG;
        READ_UINT8(cstat,msg->reply);
        READ_UINT8(err,msg->reply);
        if (err == TOX_ERR_FRIEND_QUERY_OK && fnum<UINT32_MAX){
          m_set_friend_connectionstatus(m,fnum, cstat, userdata);
          LOGGER_DEBUG(m->log, "Setting connection status for friend %d to %d." ,fnum, cstat);
        } else {
          LOGGER_DEBUG(m->log, "Problems with ATTOX_FRIEND_GET_CONNECTION_STATUS_ID, err %d" ,err);
        }
        return true;
      }
      return false; break;
    }
    case ATTOX_SELF_GET_CONNECTION_STATUS_ID:
    {
      if(msg->reply != NULL){
        uint8_t cval;
        INIT_READ_MSG;
        READ_UINT8(cval,msg->reply);
        connection_status_cb(m, cval, userdata);
        LOGGER_DEBUG(m->log, "Setting connection status to %d.", cval);
        return true;
      }
      return false;
    }
    case ATTOX_FRIEND_SEND_MESSAGE_ID:
    {
      if(msg->reply != NULL){
        
        
        uint32_t fnum;
        bool typing;
        INIT_READ_MSG;
        rd = msg2fnum(rd, &fnum, msg, m);
        
        if (fnum<UINT32_MAX) {
          uint8_t errval;
          bool iskeeping;
          INIT_READ_MSG;
          READ_UINT8(errval,msg->reply);
          READ_BOOL(iskeeping,msg->reply);
          m_set_server_read_receipt(m, fnum , msg->reply->id, iskeeping);
        }
        
        return true;
      }
      return false;
    }
    
// Callbacks

    case ATTOX_SELF_CONNECTION_STATUS_CB_ID:
    {
      assert(msg->reply == NULL);
      uint8_t cval;
      INIT_READ_MSG;
      READ_UINT8(cval,msg);
      connection_status_cb(m, cval, userdata);
      LOGGER_DEBUG(m->log, "Setting connection status to %d.", cval);
      return true;
      
    }
    case ATTOX_FRIEND_REQUEST_CB_ID:
    {
      assert(msg->reply == NULL);
      uint8_t * pk;
      size_t pksize;
      uint8_t * message;
      size_t messagel;
      
      INIT_READ_MSG;
      
      READ_UINT8STAR_REF(pk, pksize ,msg);
      if (pksize != TOX_PUBLIC_KEY_SIZE) {
        LOGGER_DEBUG(m->log, "Invalid Public key received!");
      }
      
      READ_UINT8STAR_REF(message, messagel ,msg);
      
      friendreq_handlepacket(&(m->fr), pk, message, messagel, userdata);
      //*(m->friend_req)(&(m->fr), pk, message, messagel);

      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_REQUEST_CB_ID");
      return true;
    }
    case ATTOX_FRIEND_MESSAGE_CB_ID:
    {
      assert(msg->reply == NULL);
      assert(msg->wants_reply == false);
      uint32_t fnum;
      uint8_t typ;
      uint8_t * mess;
      size_t messl;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_UINT8(typ,msg);
      READ_UINT8STAR_REF(mess, messl, msg);

      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_MESSAGE_CB_ID, friend %d wrote %s ; type %d.", fnum, mess, typ);
      /* inform of namechange before we overwrite the old name */
      if (fnum<UINT32_MAX)
        m_set_friend_message(m, fnum, typ, mess, messl, userdata);
      return true;
    }
    case ATTOX_FRIEND_NAME_CB_ID:
    {
      assert(msg->reply == NULL);
      uint32_t fnum;
      uint8_t * name;
      size_t namel;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_UINT8STAR_REF(name, namel, msg);

      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_NAME_CB_ID, Setting friend %d name to %s.", fnum, name);
      
      if(fnum<UINT32_MAX)
        m_set_friendname(m, fnum, name, namel,userdata);
      return true;
    }
    case ATTOX_FRIEND_STATUS_MESSAGE_CB_ID:
    {
      assert(msg->reply == NULL);
      uint32_t fnum;
      uint8_t * stat_msg;
      size_t stat_msgl;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_UINT8STAR_REF(stat_msg, stat_msgl, msg);

      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_STATUS_MESSAGE_CB_ID, Setting friend %d name to %s.", fnum, stat_msg);
      
      if(fnum<UINT32_MAX)
        m_set_friend_statusmessage(m, fnum, stat_msg, stat_msgl, userdata);
      return true;
    }
    case ATTOX_FRIEND_STATUS_CB_ID:
    {
      assert(msg->reply == NULL);
      uint32_t fnum;
      uint8_t status;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_UINT8(status,msg);

      LOGGER_DEBUG(m->log, "Setting friend %" PRIu32 " status to %d.", fnum, status);
      
      if(fnum<UINT32_MAX)
        m_set_friend_userstatus(m, fnum, status, userdata);
      return true;
    }
    case ATTOX_FRIEND_CONNECTION_STATUS_CB_ID:
    {
      uint32_t fnum;
      uint8_t status;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_UINT8(status,msg);

      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_CONNECTION_STATUS_CB_ID Setting friend %" PRIu32 " conn status to %d.", fnum, status);

      if(fnum<UINT32_MAX)
        m_set_friend_connectionstatus(m, fnum, status, userdata);
      
      return true;
    }
    case ATTOX_FRIEND_READ_RECEIPT_CB_ID:
    {
      uint32_t fnum;
      uint32_t msgid;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_UINT32(msgid,msg);

      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_READ_RECEIPT_CB_ID friend %d receipt %d.", fnum, msgid);
      
      if(fnum<UINT32_MAX)
        m_set_friend_read_receipt(m,fnum, msgid);
      
      return true;
    }
    case ATTOX_FRIEND_TYPING_CB_ID:
    {
      uint32_t fnum;
      bool typing;
      INIT_READ_MSG;
      rd = msg2fnum(rd, &fnum, msg, m);
      READ_BOOL(typing, msg);
      
      LOGGER_DEBUG(m->log, "ATTOX_FRIEND_TYPING_CB_ID friend %d receipt %d.", fnum, typing);
      
      if (fnum<UINT32_MAX)
        m_set_friend_typing(m,fnum,typing,userdata);
      return true;
    }
    
//
    default:
    {
      LOGGER_DEBUG(m->log, "Received unhandled msg type: %d. Removed.",msg->type);
      return true;
    }
    
  }
  
}
