#include "dyad.h"
#include "session.h"

void send_queued_msgs_to_all_sessions(dyad_Stream * s, session_t *sessions_head);

void send_queued_msgs_to_session(dyad_Stream * s, session_t *session);

void send_reply_msg_to_session(dyad_Stream * s, msg_t* msg, uint16_t session_id);
