#ifndef _INTERPRETER_H
#define _INTERPRETER_H
#include "msg.h"
#include "net.h"
#include "utlist.h"
#include "mess.h"

bool do_it(Mess * m, msg_t * msg, void * userdata);
void deal_with_messages(Mess * m, void * userdata);
#endif
