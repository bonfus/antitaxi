#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include "utlist.h"
#include "endian.h"
#include "msg.h"
#include "protocol.h"


// THIS MUST BE REMOVED
#include <stdio.h>
#define exit(x) printf("EXIT CACCA! %s %d, %d\n",__FILE__,__LINE__,x)


size_t uint8_to_data(uint8_t v, uint8_t * data) {
  if (data) {
    *data++=PRTCL_UINT8;
    memcpy(data,&v, sizeof(uint8_t));
    return sizeof(uint8_t)+sizeof(uint8_t);
  }
  return 0;
}


size_t data_to_uint8(uint8_t * v, uint8_t * data) {
  if (data) {
    if (*data == PRTCL_UINT8) {
      *v = *(data+1);
    } else {
      exit(1);
    }
    return sizeof(uint8_t)+sizeof(uint8_t);
  }
  return 0;
}

size_t bool_to_data(bool v, uint8_t * data) {
  if (data) {
    *data++=PRTCL_BOOL;
    uint8_t uv = (uint8_t) v;
    memcpy(data,&uv, sizeof(uint8_t));
    return sizeof(uint8_t)+sizeof(uint8_t);
  }
  return 0;
}

size_t data_to_bool(bool * v, uint8_t * data) {
  if (data) {
    if (*data == PRTCL_BOOL) {
      *v = (bool) *(data+1);
    } else {
      exit(1);
    }
    return sizeof(uint8_t)+sizeof(uint8_t);
  }
  return 0;
}



size_t uint16_to_data(uint16_t v, uint8_t * data) {
  if (data) {
    *data++=PRTCL_UINT16;
    uint16_t lv = sys_htols(v);
    memcpy(data,&lv, sizeof(uint16_t));
    return sizeof(uint8_t) + sizeof(uint16_t);
  }
  return 0;
}

size_t data_to_uint16(uint16_t *v, uint8_t * data) {
  if (data) {
    if (*data == PRTCL_UINT16) {
      uint16_t lv = *(data+1) | (*(data+2) << 8); 
      *v = sys_ltohs(lv);
    } else {
      exit(1);
    }
    return sizeof(uint8_t) + sizeof(uint16_t);
  }
  return 0;
}

size_t uint32_to_data(uint32_t v, uint8_t * data) {
  if (data) {
    *data++=PRTCL_UINT32;
    // convert to little
    uint32_t lv = sys_htoll(v);
    // use new lv
    memcpy(data,&lv, sizeof(uint32_t));
    return sizeof(uint8_t) + sizeof(uint32_t);
  }
  return 0;
}

size_t data_to_uint32(uint32_t * v, uint8_t * data) {
  if (data) {
    if (*data == PRTCL_UINT32) {
      uint32_t lv = *(data+1) | (*(data+2) << 8) | (*(data+3) << 16) | (*(data+4) << 24);
      *v = sys_ltohl(lv);
    } else {
      exit(1);
    }
    return sizeof(uint8_t) + sizeof(uint32_t);
  }
  return 0;
}



size_t sizet_to_data(size_t v, uint8_t * data) {
  if (data) {
    *data++=PRTCL_SIZET;
    // size_t is platform dependent...cast to init32_t and hope.
    uint32_t s = (uint32_t) v;
    uint32_t ls = sys_htoll(s);
    memcpy(data,&ls, sizeof(uint32_t));
    return sizeof(uint8_t) + sizeof(uint32_t);
  }
  return 0;
}

size_t data_to_sizet(size_t * v, uint8_t * data) {
  if (data) {
    if (*data == PRTCL_SIZET) {
      uint32_t lv = *(data+1) | (*(data+2) << 8) | (*(data+3) << 16) | (*(data+4) << 24);
      *v = (size_t) sys_ltohl(lv);
    } else {
      exit(*data);
    }
    return sizeof(uint8_t) + sizeof(uint32_t);
  }
  return 0;
}

size_t uint8star_to_data(const uint8_t * v, size_t length, uint8_t * data) {
  if (data) {
    *data=PRTCL_UINT8STAR;
    uint32_t len_u32 = (uint32_t) length;
    uint32_t le_len_u32 = sys_htoll(len_u32);
    memcpy(data+1,&le_len_u32, sizeof(uint32_t));
    
    memcpy(data+5, v, length);
    
    return sizeof(uint8_t) + sizeof(uint32_t) + length;
  }
  return 0;
}

size_t data_to_uint8star_copy(uint8_t ** v, size_t * v_len, uint8_t * data, bool alloc) {
  if (data) {
    if (*data == PRTCL_UINT8STAR) {
      uint32_t lv = *(data+1) | (*(data+2) << 8) | (*(data+3) << 16) | (*(data+4) << 24);
      size_t data_len = (size_t) sys_ltohl(lv);
      
      if (alloc)
        *v = realloc(*v, sizeof(uint8_t)*data_len);  // free is done by realloc
      memcpy(*v,data+(PRTCL_DATA_TYPE_LEN+sizeof(uint32_t)), data_len);
      *v_len = data_len;
      return (PRTCL_DATA_TYPE_LEN+sizeof(uint32_t) + data_len);
    } else {
      *v = NULL;
      *v_len = 0;
      exit(1);
    }
  }
  return 0;
}

// returns data read
size_t data_to_uint8star_ref(uint8_t ** v, size_t * v_len, uint8_t * data) {
  if (data) {
    if (*data == PRTCL_UINT8STAR) {
      uint32_t lv = *(data+1) | (*(data+2) << 8) | (*(data+3) << 16) | (*(data+4) << 24);
      size_t data_len = (size_t) sys_ltohl(lv);
      
      *v = data+PRTCL_DATA_TYPE_LEN+sizeof(uint32_t);
      *v_len = data_len;
      return (PRTCL_DATA_TYPE_LEN + sizeof(uint32_t) + data_len);
    } else {
      *v = NULL;
      *v_len=0;
      exit(*data);
    }
  }
  return 0;
}



//size_t allocate_message ( msg_t * msg, ... )
//{
//
//  if (msg==NULL)
//    return 0;
//
//  size_t msg_len = MSG_HEADER_LEN;
//  
//  va_start( argptr, msg );   
//  int type = va_arg( argptr, int );
//  size_t field_len = 0;
//  while(type != 0) {
//    switch(type) {
//      case: PRTCL_UINT8:
//        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint8_t);
//        break;
//      case: PRTCL_UINT16:
//        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint16_t);
//        break;
//      case: PRTCL_UINT32:
//        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t);
//        break;
//      case: PRTCL_UINT8STAR:
//        field_len = va_arg( argptr, size_t );
//        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t) + field_len;
//        break;
//      case: PRTCL_SIZET:
//        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t);
//        break;
//      case: PRTCL_BOOL:
//        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint8_t);
//        break;
//      case default:
//        exit(998);
//    }
//  }
//  
//  va_end ( argptr );
//  
//  assert(msg->data == NULL);
//  assert(msg->data_len == 0);
//  
//  msg->data=calloc(msg_len, sizeof(uint8_t));
//  
//  assert(msg->data != NULL);
//  // shift to reserve space for header
//  msg->data_len = msg_len;
//  return msg_len;
//}

msg_t * new_empty_message(uint16_t type, bool wants_reply, ...){
  
  msg_t * msg = calloc(1,sizeof(msg_t));
  assert(msg != NULL);
  
  msg->type = type;
  msg->sess_id = UINT16_MAX;          // not assigned
  msg->id = rand()%(UINT32_MAX-1);    // random
  msg->ref_count=0;
  msg->is_reply = false;
  msg->wants_reply=wants_reply;
  msg->reply=NULL;
  
  
  // now allocate message
  va_list argptr;
  size_t msg_len = MSG_HEADER_LEN;
  
  va_start( argptr, wants_reply );   
  int mtype = va_arg( argptr, int );
  size_t field_len = 0;
  
  while(mtype != NO_MORE_FIELDS) {
    switch(mtype) {
      case PRTCL_UINT8:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint8_t);
        break;
      case PRTCL_UINT16:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint16_t);
        break;
      case PRTCL_UINT32:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t);
        break;
      case PRTCL_UINT8STAR:
        field_len = va_arg( argptr, size_t );
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t) + field_len;
        break;
      case PRTCL_SIZET:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t);
        break;
      case PRTCL_BOOL:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint8_t);
        break;
      default:
        exit(998);
    }
    mtype = va_arg( argptr, int );
  }
  va_end ( argptr );
  
  
  msg->data=calloc(msg_len, sizeof(uint8_t));
  assert(msg->data != NULL);
  msg->data_len = msg_len;
  
  return msg;
}



msg_t * new_reply_message(msg_t * orig, ...){
  
  msg_t * msg = calloc(1,sizeof(msg_t));
  assert(msg!=NULL);
  
  msg->type = orig->type;
  msg->id = orig->id;
  msg->sess_id = orig->sess_id;
  msg->ref_count=0;
  msg->is_reply=true;
  msg->wants_reply=false;
  msg->reply=NULL;
  
  // now allocate message
  va_list argptr;
  size_t msg_len = MSG_HEADER_LEN;
  
  va_start( argptr, orig );   
  int mtype = va_arg( argptr, int );
  size_t field_len = 0;
  
  while(mtype != NO_MORE_FIELDS) {
    switch(mtype) {
      case PRTCL_UINT8:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint8_t);
        break;
      case PRTCL_UINT16:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint16_t);
        break;
      case PRTCL_UINT32:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t);
        break;
      case PRTCL_UINT8STAR:
        field_len = va_arg( argptr, size_t );
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t) + field_len;
        break;
      case PRTCL_SIZET:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint32_t);
        break;
      case PRTCL_BOOL:
        msg_len += PRTCL_DATA_TYPE_LEN + sizeof(uint8_t);
        break;
      default:
        exit(998);
    }
    mtype = va_arg( argptr, int );
  }
  va_end ( argptr );
  
  
  msg->data=calloc(msg_len, sizeof(uint8_t));
  assert(msg->data != NULL);
  msg->data_len = msg_len;
  
  return msg;
}


msg_t * write_msg_header(uint16_t sess_id, msg_t * msg) {
  
  // this function writes the first MSG_HEADER_LEN bytes
  
  printf("sess: %d type: %d reply: %d id: %d len: %d\n", sess_id, msg->type, msg->is_reply, msg->id, msg->data_len);
  
  size_t wd = uint16_to_data(sess_id, msg->data);
  wd = wd + uint16_to_data(msg->type, msg->data+wd);
  wd = wd + bool_to_data(msg->is_reply, msg->data + wd);
  wd = wd + uint32_to_data(msg->id, msg->data + wd);
  uint32_to_data(msg->data_len, msg->data + wd);
  
  return msg;
  
}

msg_t * data2msg(uint8_t * data, size_t data_len) {
  
  
  
  if (data_len < MSG_HEADER_LEN) { // message is too short!
    
    return NULL;
  }
  
  msg_t * msg = calloc(1,sizeof(msg_t));
  
  // rd stores already read data
  size_t rd = 0;
  rd = rd + data_to_uint16(&(msg->sess_id), data + rd);  
  rd = rd + data_to_uint16(&(msg->type), data + rd);
  rd = rd + data_to_bool(&(msg->is_reply), data + rd);
  rd = rd + data_to_uint32(&(msg->id), data + rd);
  rd = rd + data_to_uint32(&(msg->data_len), data + rd);
  
  
  
  //printf("msg-<data_len: %d...\n", msg->data_len);
  
  if (data_len < msg->data_len){ // message is too short!
    free(msg);
    return NULL;
  }
  
  msg->data=calloc(msg->data_len, sizeof(uint8_t));
  
  // check messagedata
  if (msg->data==NULL){
    // FAIL exit()
    free(msg);
    return NULL;
  }
  
  memcpy(msg->data, data, msg->data_len);
  return msg;
}


void free_msg(msg_t * msg) {
  if (msg->data)
    free(msg->data);
    
  if (msg->reply){
    if (msg->reply->data) {
      free(msg->reply->data);
    }
    free(msg->reply);
  }
  free(msg);
}
