#include <stdlib.h>
#include "crypto_core.h"
#include <stdbool.h>
#include "session.h"
#include "utlist.h"
/* sessions */

static int session_cmp(session_t *a, session_t *b) {
    return (a->id == b->id) ? 0 : 1;
}

session_t * find_session_by_id(session_t *sessions_head, uint16_t id) {
  session_t like;
  session_t * elt = NULL;
  
  like.id = id;
  
  
  DL_SEARCH(sessions_head,elt,&like,session_cmp);
  return elt;
}

// UINT16_MAX defines a uninitialized sesison.
session_t * client_session(uint16_t id, unsigned char otherpk[])
{
  session_t * sess = calloc(1, sizeof(session_t));
  
  sess->msgs = NULL;
  sess->id = id;
  sess->bootstrap_done = false;
  
  // generate keys
  crypto_box_keypair(sess->mypk, sess->mysk);
  memcpy(sess->otherpk, otherpk, crypto_box_PUBLICKEYBYTES);
  return sess;
}

session_t * server_session(uint16_t id, unsigned char pk[], unsigned char sk[], unsigned char cpk[])
{
  session_t * sess = calloc(1, sizeof(session_t));
  sess->msgs = NULL;
  sess->bootstrap_done = false;
  // defines a new session ID.
  sess->id = id;
  memcpy(sess->mypk, pk, crypto_box_PUBLICKEYBYTES);
  memcpy(sess->mysk, sk, crypto_box_SECRETKEYBYTES);
  memcpy(sess->otherpk, cpk, crypto_box_PUBLICKEYBYTES);
  return sess;
}

uint16_t get_valid_id(session_t * sess_head){
  uint16_t val;
  session_t * sess;
  bool ok = false;
  while (!ok) {
    val = rand()%(UINT16_MAX-1);
    ok = true;
    DL_FOREACH(sess_head, sess) {
      if (sess->id == val) {
        ok = false;
        break;
      }
    }
  }
  return val;
}

void clear_session(session_t * sess)
{
  msg_ref_t * mref,*mreftmp;
  LL_FOREACH_SAFE(sess->msgs, mref, mreftmp){
    rem_message_ref(mref);
    LL_DELETE(sess->msgs,mref);
    free(mref);
  }
  
}

int count_unsent_messages(session_t * sess){
  int c = 0;
  msg_ref_t * mref;
  LL_FOREACH(sess->msgs, mref){
    if (mref->send == true) {
      c +=1;
    }
  }
  return c;
}

int count_unprocessed_messages(session_t * sess){
  int c = 0;
  msg_ref_t * mref;
  LL_FOREACH(sess->msgs, mref){
    assert(mref->msg != NULL);
    if (mref->msg->wants_reply == true && mref->msg->reply==NULL) {
      c +=1;
    }
  }
  return c;
}

msg_ref_t * new_message_ref(msg_t * msg) {
  msg_ref_t * msgr = (msg_ref_t*) calloc(1,sizeof(msg_ref_t));
  msgr->msg = msg;
  // increment references to msg
  msg->ref_count = msg->ref_count + 1;
  
  // this is a true message
  msgr->send=true;
  
  return msgr;
}

msg_ref_t * new_callback_ref(msg_t * msg) {
  msg_ref_t * msgr = (msg_ref_t*) calloc(1,sizeof(msg_ref_t));
  msgr->msg = msg;
  msg->ref_count = msg->ref_count + 1;
  
  // this is a callback, it should not be sent
  msgr->send=false;
  return msgr;
}

msg_ref_t * rem_message_ref(msg_ref_t * msgr) {
  msg_t * msg = msgr->msg;
  msg->ref_count = (msg->ref_count>0) ? msg->ref_count -1 : 0;
  if (msg->ref_count == 0) {
    free_msg(msg);
  }
  return msgr;
}

/* end sessions */
