#ifndef _DATA_BUF_H
#define _DATA_BUF_H
#include "pkt.h"
#include "endian.h"

#define FRAME_HEAD 7
#define MAX_BUF_SIZE (MAX_CRYPTO_REQUEST_SIZE+FRAME_HEAD)

typedef struct data_buff_struct {
  uint8_t leftover[MAX_BUF_SIZE];
  size_t current_pos;
  size_t leftover_pos;
} data_buf_t;


size_t buf_push_pull(data_buf_t * buf, pkt_t * pkt, size_t pos, uint8_t * data, size_t data_len);
//size_t buf_pull(data_buf_t * buf, pkt_t * pkt);
void buf_reset(data_buf_t * buf);
#endif
