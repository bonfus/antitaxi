#include "slaimer.h"
#include "utlist.h"

bool slaimer_message(Mess * m, const uint8_t * msg, size_t len)
{
  static bool gave_password=false;
  
  char cmsg[501];
  if (len>500) {
    memcpy(cmsg, msg, 500*sizeof(uint8_t));
    cmsg[500]='\0';
  } else {
    memcpy(cmsg, msg, len);
    cmsg[len]='\0';
  }
  
  LOGGER_DEBUG(m->log, "MESSAGE FOR SLAIMER: %s ", cmsg);
  
  char* tok=strtok(cmsg," ");
  
  if (tok == NULL)
    return false;
  
  if (strncmp(tok,"pass",4)==0) {
    tok=strtok(NULL," ");
    if (tok) {
      if (strncmp(tok, m->net->password, strlen(m->net->password))==0) {
        gave_password=true;
        slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
        strcpy(sl_entry->msg, "Password is valid! Go on...");
        LL_APPEND(m->slaimer_log, sl_entry);
        return true;
      }
      slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
      strcpy(sl_entry->msg, "Password not valid!");
      LL_APPEND(m->slaimer_log, sl_entry);
            
      return false;
    }
    return false;
  }
  if (strncmp(tok,"quit",4)==0) {
    gave_password=false;
    slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
    strcpy(sl_entry->msg, "Logged out from set session");
    LL_APPEND(m->slaimer_log, sl_entry);
    return true;
  } 
  
  if (strncmp(tok,"set",3)==0) {
    tok=strtok(NULL," ");
    
    if (tok == NULL)
      return false;
    
    
    if (gave_password == false) {
      slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
      strcpy(sl_entry->msg, "Please give password!");
      LL_APPEND(m->slaimer_log, sl_entry);
      return false;
    }
    
    if (strncmp(tok,"pass",4)==0) {
      tok=strtok(NULL," ");
      if (tok) {
        strcpy(m->net->password, tok);

        slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
        strcpy(sl_entry->msg, "Password set");
        LL_APPEND(m->slaimer_log, sl_entry);
        return true;
      }
    }
    
    if (strncmp(tok,"host",4)==0) {
      tok=strtok(NULL," ");
      if (tok) {
        strcpy(m->net->host, tok);
        slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
        strcpy(sl_entry->msg, "Host set");
        LL_APPEND(m->slaimer_log, sl_entry);
        return true;
      }
    }
    
    if (strncmp(tok,"port",4)==0) {
      tok=strtok(NULL," ");
      if (tok) {
        int v = strtol(tok,NULL,10);
        if (v>0 && v < UINT16_MAX) {
          m->net->port = v;
          slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
          strcpy(sl_entry->msg, "Port set");
          LL_APPEND(m->slaimer_log, sl_entry);
          return true;
        }
        return false;
      }
    }
    
    if (strncmp(tok,"srvpk",5)==0) {
      tok=strtok(NULL," ");
      if (tok) {
        uint8_t address[crypto_box_PUBLICKEYBYTES] = {0};
        int ret = sodium_hex2bin(address, crypto_box_PUBLICKEYBYTES, tok,
                        strlen(tok), NULL, NULL, NULL);
        
        if(ret==0) {
          memcpy(m->net->sessions->otherpk, address, crypto_box_PUBLICKEYBYTES);
  
          slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
          strcpy(sl_entry->msg, "Server PK set");
          LL_APPEND(m->slaimer_log, sl_entry);
          return true;
        } else {
          slaimer_msgs_t * sl_entry = malloc(sizeof(slaimer_msgs_t));
          strcpy(sl_entry->msg, "Server PK not parsed!");
          LL_APPEND(m->slaimer_log, sl_entry);
        }
      }
    }
  }
}
