#ifndef _NET_H
#define _NET_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdint.h>
#include <sodium.h>
#include "dyad.h"
#include "endian.h"
#include "tox.h"
#include "tox_defs.h"
#include "pkt.h"
#include "protocol.h"
#include "session.h"
#include "logger.h"
#include "data_buf.h"


#define NET_IDLE_ITERATION_TIME 0.1

//#define TOX_MAX_NAME_LENGTH MAX_NAME_LENGTH
//#define TOX_MAX_STATUS_MESSAGE_LENGTH MAX_STATUSMESSAGE_LENGTH

#define MAX_PASSWORD_LEN 20
#define FILE_ID_LENGTH 32

typedef enum AT_CONNECTION_STATUS
{
  NOT_CONNECTED = -3,
  CONNECTED = 0
} AT_CONNECTION_STATUS;

typedef struct net_s{
  // stuff of antitaxi
  session_t * sessions;
  dyad_Stream *s;
  unsigned char password[MAX_PASSWORD_LEN+1];
  AT_CONNECTION_STATUS connection_status;
  unsigned char host[21];
  int port;
  uint16_t last_session_id;
  data_buf_t buf;
} _net;

void onDisconnect(dyad_Event *e);
void onConnect(dyad_Event *e);
void onError(dyad_Event *e);
void onData(dyad_Event *e);



#endif

