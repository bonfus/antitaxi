#ifndef _MSG_H
#define _MSG_H


#include <inttypes.h>
#include <stdbool.h>
#include <unistd.h>
#include "protocol.h"
#include "crypto_core.h"

// NOTA: replace sizeof(uint8_t) with PROTOCOL_DATA_TYPE_LEN
#define MSG_HEADER_SESS_ID_LEN sizeof(uint8_t) + sizeof( uint16_t ) // sess_id
#define MSG_HEADER_TYPE_LEN sizeof(uint8_t) + sizeof( uint16_t )  // type
#define MSG_HEADER_IS_REPLY_LEN sizeof(uint8_t) + sizeof( uint8_t ) //is _reply
#define MSG_HEADER_ID_LEN sizeof(uint8_t) + sizeof( uint32_t ) //id
#define MSG_HEADER_DATA_LEN_LEN sizeof(uint8_t) + sizeof( uint32_t ) // data_len

#define MSG_HEADER_LEN (MSG_HEADER_SESS_ID_LEN + MSG_HEADER_TYPE_LEN + MSG_HEADER_IS_REPLY_LEN + MSG_HEADER_ID_LEN + MSG_HEADER_DATA_LEN_LEN)


#define MAX_ANTITAXI_DATA_SIZE (MAX_CRYPTO_REQUEST_SIZE-(1 + 2*crypto_box_PUBLICKEYBYTES + crypto_box_NONCEBYTES + 1 + crypto_box_MACBYTES))
#define MAX_ANTITAXI_DATA_REQUEST_SIZE (MAX_ANTITAXI_DATA_SIZE-(MSG_HEADER_LEN))



// read helpers
#define INIT_READ_MSG size_t rd = MSG_HEADER_LEN, add = 0;

#define READ_UINT8STAR_REF(_v,_vl,_m)     add = data_to_uint8star_ref(&_v,&_vl,_m->data+rd);\
                                          assert(add>0); rd += add;
                                          
#define READ_UINT8STAR_CPY(_v,_vl,_m)     add = data_to_uint8star_copy(&_v,&_vl,_m->data+rd,false);\
                                          assert(add>0); rd += add;
                                          
#define READ_UINT8STAR_ALLOC(_v,_vl,_m)   add = data_to_uint8star_copy(&_v,&_vl,_m->data+rd,true);\
                                          assert(add>0); rd += add;

#define READ_SIZET(_v,_m)                 add = data_to_sizet(&_v, _m->data+rd);\
                                          assert(add>0); rd += add;

#define READ_UINT8(_v,_m)                 add = data_to_uint8(&_v, _m->data+rd);\
                                          assert(add>0); rd += add;

#define READ_UINT16(_v,_m)                 add = data_to_uint16(&_v, _m->data+rd);\
                                          assert(add>0); rd += add;

#define READ_UINT32(_v,_m)                 add = data_to_uint32(&_v, _m->data+rd);\
                                          assert(add>0); rd += add;

#define READ_BOOL(_v,_m)                   add = data_to_bool(&_v, _m->data+rd);\
                                          assert(add>0); rd += add;

// write helpers

#define INIT_WRITE_MSG size_t wd = MSG_HEADER_LEN, addw = 0;

#define WRITE_UINT8STAR(_v,_l,_m)            addw = uint8star_to_data(_v, _l, _m->data+wd); \
                                          assert(addw != 0); wd += addw; assert(_m->data_len >= wd); assert(_m->data_len <= MAX_ANTITAXI_DATA_SIZE);

#define WRITE_SIZET(_v,_m)                  addw = sizet_to_data(_v, _m->data+wd); \
                                          assert(addw != 0); wd += addw; assert(_m->data_len >= wd); assert(_m->data_len <= MAX_ANTITAXI_DATA_SIZE);

#define WRITE_UINT8(_v,_m)                  addw = uint8_to_data(_v, _m->data+wd); \
                                          assert(addw != 0); wd += addw; assert(_m->data_len >= wd); assert(_m->data_len <= MAX_ANTITAXI_DATA_SIZE);

#define WRITE_UINT16(_v,_m)                 addw = uint16_to_data(_v, _m->data+wd); \
                                          assert(addw != 0); wd += addw; assert(_m->data_len >= wd); assert(_m->data_len <= MAX_ANTITAXI_DATA_SIZE);

#define WRITE_UINT32(_v,_m)                 addw = uint32_to_data(_v, _m->data+wd); \
                                          assert(addw != 0); wd += addw; assert(_m->data_len >= wd); assert(_m->data_len <= MAX_ANTITAXI_DATA_SIZE);

#define WRITE_BOOL(_v,_m)                   addw = bool_to_data(_v, _m->data+wd); \
                                          assert(addw != 0); wd += addw; assert(_m->data_len >= wd); assert(_m->data_len <= MAX_ANTITAXI_DATA_SIZE);
        



#define NO_FIELDS 99
#define NO_MORE_FIELDS NO_FIELDS // just dor reading

typedef struct msg_struct {
  uint16_t sess_id;
  uint16_t type;
  uint32_t id;
  uint32_t data_len;
  uint8_t * data;
  // information regarding message
  uint16_t ref_count;
  bool wants_reply;
  bool is_reply;
  struct msg_struct * reply;
} msg_t;


/* data tools */
size_t data_to_uint8(uint8_t * v, uint8_t * data);
size_t data_to_bool(bool * v, uint8_t * data);
size_t data_to_uint16(uint16_t *v, uint8_t * data);
size_t data_to_uint32(uint32_t * v, uint8_t * data);
size_t data_to_sizet(size_t * v, uint8_t * data);
size_t data_to_uint8star_ref(uint8_t ** v, size_t * v_len, uint8_t * data);
size_t data_to_uint8star_copy(uint8_t ** v, size_t * v_len, uint8_t * data, bool alloc);


size_t uint8_to_data(uint8_t v, uint8_t * data);
size_t uint16_to_data(uint16_t v, uint8_t * data);
size_t uint32_to_data(uint32_t v, uint8_t * data);
size_t sizet_to_data(size_t v, uint8_t * data);
size_t uint8star_to_data(const uint8_t * v, size_t length, uint8_t * data);
size_t bool_to_data(bool v, uint8_t * data);

/* msg utility functions */
msg_t * new_empty_message(uint16_t type, bool wants_reply, ...);
msg_t * new_reply_message(msg_t * orig, ...);


msg_t * write_msg_header(uint16_t sess_id, msg_t * msg);
msg_t * data2msg(uint8_t * data, size_t data_len);
void free_msg(msg_t * msg);



#endif
