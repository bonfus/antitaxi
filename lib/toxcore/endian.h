/** \file endian.h
  *
  * \brief Describes functions exported by endian.c
  *
  * This file is licensed as described by the file LICENCE.
  */

#ifndef ENDIAN_H_INCLUDED
#define ENDIAN_H_INCLUDED

#include <inttypes.h>

uint16_t sys_htols(uint16_t v);
uint32_t sys_htoll(uint32_t v);
uint16_t sys_ltohs(uint16_t v);
uint32_t sys_ltohl(uint32_t v);
uint64_t sys_htonll(uint64_t v);
uint64_t sys_ntohll(uint64_t v);

#endif // #ifndef ENDIAN_H_INCLUDED

