#ifndef _PKT_H
#define _PKT_H
#include <sodium.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#include "crypto_core.h"
#include "msg.h"
#include "session.h"


#define PKTID_FROM_SERVER 98
#define PKTID_FROM_CLIENT 99

#if defined(_SERVER)
#define PKTID PKTID_FROM_SERVER
#elif defined(_CLIENT)
#define PKTID PKTID_FROM_CLIENT
#else
#error define client or server
#endif

typedef struct pkt_struct {
  uint8_t * crypt_data; //[MAX_CRYPTO_REQUEST_SIZE]
  size_t crypt_data_len;
} pkt_t;

msg_t * pkt2msg( unsigned char self_pk[], unsigned char self_sk[], 
              pkt_t pkt, unsigned char * pub_key);
bool msg2pkt(session_t * sess, msg_t * msg, pkt_t * pkt);


#endif
