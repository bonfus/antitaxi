#include <assert.h>
#include "data_buf.h"
#include "crc.h"


static inline size_t min(size_t a, size_t b) { return (a < b) ? a : b;}

static size_t is_head(uint8_t * data, size_t data_len) {
  if (data_len < FRAME_HEAD)
    return 0;
  //printf("%d %d %d %d\n", *data, *(data+1),*(data+2),*(data+3));
  if ( *data != THATS_MAGIC || 
      *(data+1) != THATS_MAGIC || 
      *(data+2) != THATS_MAGIC || 
      *(data+3) != THATS_MAGIC) {
    return 0;
  }
  
  uint16_t _tmp = *(data+4) | (*(data+5) << 8); 
  size_t crypt_len = sys_ltohs(_tmp); // check len!!
  if (crypt_len > MAX_CRYPTO_REQUEST_SIZE) {
    return 0;
  }
  return crypt_len;
}

static bool is_possibly_head(uint8_t * data, size_t data_len) {
  
  size_t i=0;
  for (i=0; i<data_len;i++) {
    switch (i)
    {
      case 0:
      case 1:
      case 2:
      case 3:
        if (*(data+i) != THATS_MAGIC) {
          return false;
        }
        break;
      case 4:
        break;
      case 5:
      {
        uint16_t _tmp = *(data+4) | (*(data+5) << 8); 
        size_t crypt_len = sys_ltohs(_tmp); // check len!!
        if (crypt_len > MAX_CRYPTO_REQUEST_SIZE) {
          return false;
        }
      }
      case 6:
        break;
      default:
        return is_head(data, data_len);
    }
  }
  return true;
}

static size_t is_packet(uint8_t * data, size_t data_len) {
  if (data_len < FRAME_HEAD)
    return 0;
    
  if ( *data != THATS_MAGIC || 
      *(data+1) != THATS_MAGIC || 
      *(data+2) != THATS_MAGIC || 
      *(data+3) != THATS_MAGIC) {
    return 0;
  }
  
  uint16_t _tmp = *(data+4) | (*(data+5) << 8); 
  size_t crypt_len = sys_ltohs(_tmp); // check len!!
  if (crypt_len > MAX_CRYPTO_REQUEST_SIZE) {
    return 0;
  }
  uint8_t crypt_crc = *(data+6);
  uint8_t valid_crc = crc8_check(data+7, crypt_len, crypt_crc);
  if (valid_crc!=0) {
    return 0;
  }
  return crypt_len;
}

void buf_reset(data_buf_t * buf){
  buf->leftover_pos=0;
  memset(buf->leftover, 0 , MAX_BUF_SIZE);
  printf("BUF===RESET;\n");
}

// returns data read
size_t buf_push_pull(data_buf_t * buf, pkt_t * pkt, size_t pos, uint8_t * data, size_t data_len)
{
  // problematic restart, clear everything!!
  if (pos > 0 && buf->leftover_pos > 0) {
    buf_reset(buf);
    assert(false);  // to be removed
  }
  printf("BUF===Call %d %d \n",pos, data_len);
  assert(buf->leftover[0] == 0 || buf->leftover[0]== THATS_MAGIC);
  
  // general checks
  if (buf->leftover_pos == 0) {  // we have nothing left from previous call
    // find the head!!
    size_t start_pos=pos;
    size_t i=pos;
    size_t len = 0;
    for (; i < data_len; i++) {
      len = is_head(data + i, data_len - i);
      if (len > 0) {
        pos = i;
        break;
      }
    }
    // we found a head
    if ( len > 0 ) {
      if ((pos + FRAME_HEAD + len) > data_len ) // this is a partial pack
      {
        printf("BUF===have partial packet, copy data and give back nothing;\n");
        memcpy(buf->leftover, data+pos, data_len - pos);
        buf->leftover_pos = data_len - pos;

        pkt->crypt_data_len = 0;
        return data_len - pos;
        
      } else { // we have the entire pkg
        printf("BUF===have entire packet;\n");
        if (is_packet(data+pos, data_len - pos)) {
          printf("BUF===have entire packet at pos %d (len %d), data from %d;\n", pos, len, pos+FRAME_HEAD);
          pkt->crypt_data_len = len;
          pkt->crypt_data = data+(pos+FRAME_HEAD);
        } else { // packet is invalid, move on anyway
          printf("BUF===have invalid packet at pos %d (len %d), data from %d;\n", pos, len, pos+FRAME_HEAD);
          pkt->crypt_data_len = 0;
          return (pos-start_pos == 0) ? 1 : pos-start_pos; // cannot trust len, pkt is invalid
                                                           // increment by bytes to where next is 
                                                           // head found. at least 1 byte.
        }
        printf("BUF===incrementing data_read by %d;\n", FRAME_HEAD + len);
        return FRAME_HEAD + len;
      }
    } else {
      printf("BUF===No head and nothing left\n");
      // we did not find the head but had nothing from previus call
      //  this means we could have an invalid packet or something with less than 7 bytes
      // check we have a partial head and eventually copy 
      // what we have and hope for the best in the future
      if (is_possibly_head(data+pos, data_len-pos)) {
        printf("BUF===copying potential head %d %d \n",data+pos, data_len - pos);
        memcpy(buf->leftover, data+pos, data_len - pos);
        buf->leftover_pos = data_len - pos;
      }

      pkt->crypt_data_len = 0;
      return data_len - pos;
    }
    
  } else {  // we have something from previous call
    assert(buf->leftover[0] == THATS_MAGIC);
    assert(buf->leftover_pos > 0);
    assert(pos==0);
    printf("BUF===something from prev call %d \n",buf->leftover_pos);
    // copy all data, (this can be improved)
    memcpy(buf->leftover+buf->leftover_pos, data, min(data_len, MAX_BUF_SIZE-buf->leftover_pos));
    size_t len = is_packet(buf->leftover, MAX_BUF_SIZE);
    if (len>0) {
      printf("BUF===addition gave a package (len %d) \n",len);      
      pkt->crypt_data_len = len;
      pkt->crypt_data = buf->leftover+FRAME_HEAD;
      printf("BUF===returning read bytes (len %d) \n",(len+FRAME_HEAD) - buf->leftover_pos);            
      size_t data_read = (len+FRAME_HEAD) - buf->leftover_pos;
      buf->leftover_pos = 0;
      return data_read;
    } else {
      printf("BUF===VERY BAD!!!\n");
      // something very bad happened!!
      buf_reset(buf);
      return 0;
    }
    
  }
  
}
