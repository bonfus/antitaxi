#include <string.h>
#include <assert.h>

#include "mess.h"
#include "msg_sender.h"
#include "util.h"
#include "utlist.h"
#include "interpreter.h"
#include "slaimer.h"



// OLD NET FUNCTIONS 

void init_net(Mess * m) {
  dyad_init();
  m->net = (_net *)calloc(1, sizeof(_net));
  m->net->host[0] = '\0';
  m->net->port = 0;
  m->net->last_session_id = UINT16_MAX; // no last session by default
  memset((m->net->buf).leftover, 0, MAX_BUF_SIZE);
  (m->net->buf).leftover_pos = 0;
  
  unsigned char nullpk[32] = {0};
  // UINT16_MAX represents an un-authenticated session
  session_t * news = client_session(UINT16_MAX, nullpk);
  DL_APPEND(m->net->sessions, news );
  
  // default password
  strcpy(m->net->password, "antitaxi");
  
  m->net->connection_status = -3;

  m->net->s = NULL;

}

void iterate_connection (Mess * m, double timeout) {
  int i;
  uint64_t t_past=0, t_start = current_time_monotonic();
  
  if (dyad_getStreamCount() == 0) {

    LOGGER_DEBUG(m->log, "Loaded info: %s %s %d %d", m->net->password, m->net->host, m->net->port, m->net->last_session_id);
    if (m->net->port == 0) {
      return;
    }
    
    // try to reconnect
    m->net->s = dyad_newStream();
    
    dyad_setUpdateTimeout(0.0);
    
    dyad_addListener(m->net->s, DYAD_EVENT_CONNECT, onConnect, (void*) m);
    dyad_addListener(m->net->s, DYAD_EVENT_CLOSE,   onDisconnect, (void*) m);
    dyad_addListener(m->net->s, DYAD_EVENT_ERROR,   onError,   (void*) m);
    dyad_addListener(m->net->s, DYAD_EVENT_DATA,    onData,    (void*) m);
    
    dyad_connect(m->net->s, m->net->host, m->net->port);
    // clear buff here!!! TODO
    buf_reset(&(m->net->buf));
    dyad_update();
  }
  
  
  while ((dyad_getStreamCount() > 0)) {
    if (m->net->s == NULL)
      break; // should not happen
      
    send_queued_msgs_to_session(m->net->s, m->net->sessions);
    dyad_update();
    if (count_unprocessed_messages(m->net->sessions) > 0) {
      dyad_setUpdateTimeout(timeout);
    }
    dyad_update();
    break;
  }
}

void shutdown_net(Mess * m) {
  if (m->net->s) {
    dyad_end(m->net->s);
    dyad_removeListener(m->net->s, DYAD_EVENT_CONNECT, onConnect,    (void*) m);
    dyad_removeListener(m->net->s, DYAD_EVENT_CLOSE,   onDisconnect, (void*) m);
    dyad_removeListener(m->net->s, DYAD_EVENT_ERROR,   onError,      (void*) m);
    dyad_removeListener(m->net->s, DYAD_EVENT_DATA,    onData,       (void*) m);
  }
  while (dyad_getStreamCount() > 0) {
    dyad_update();
  }
  dyad_shutdown();
}

// OLD NET FUNCTIONS 

static void set_friend_status(Mess *m, int32_t friendnumber, uint8_t status, void *userdata);

// friend_not_valid determines if the friendnumber passed is valid in the Mess object
static uint8_t friend_not_valid(const Mess *m, int32_t friendnumber)
{
    if ((unsigned int)friendnumber < m->numfriends) {
        if (m->friendlist[friendnumber].status != 0) {
            return 0;
        }
    }

    return 1;
}

// friend_not_valid determines if the friendnumber passed is SLAIMER in the Mess object
static uint8_t friend_is_slaimer(const Mess *m, int32_t friendnumber)
{
    if ((unsigned int)friendnumber < m->numfriends) {
      if (m->friendlist[friendnumber].status == FRIEND_SLAIMER) {
        return 1;
      }
    }
    return 0;
}

/* Set the size of the friend list to numfriends.
 *
 *  return -1 if realloc fails.
 */
static int realloc_friendlist(Mess *m, uint32_t num)
{
    if (num == 0) {
        free(m->friendlist);
        m->friendlist = NULL;
        return 0;
    }

    Friend *newfriendlist = (Friend *)realloc(m->friendlist, num * sizeof(Friend));

    if (newfriendlist == NULL) {
        return -1;
    }

    m->friendlist = newfriendlist;
    return 0;
}

/*  return the friend id associated to that public key.
 *  return -1 if no such friend.
 */
int32_t getfriend_id(const Mess *m, const uint8_t *real_pk)
{
    uint32_t i;

    for (i = 0; i < m->numfriends; ++i) {
        if (m->friendlist[i].status > 0) {
            if (id_equal(real_pk, m->friendlist[i].real_pk)) {
                return i;
            }
        }
    }

    return -1;
}

/* Copies the public key associated to that friend id into real_pk buffer.
 * Make sure that real_pk is of size CRYPTO_PUBLIC_KEY_SIZE.
 *
 *  return 0 if success.
 *  return -1 if failure.
 */
int get_real_pk(const Mess *m, int32_t friendnumber, uint8_t *real_pk)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    memcpy(real_pk, m->friendlist[friendnumber].real_pk, CRYPTO_PUBLIC_KEY_SIZE);
    return 0;
}

/*  return friend connection id on success.
 *  return -1 if failure.
 */
int getfriendcon_id(const Mess *m, int32_t friendnumber)
{
    LOGGER_ERROR(m->log, "This function should never be called!!");
    return -1;
}

/*
 *  return a uint16_t that represents the checksum of address of length len.
 */
static uint16_t address_checksum(const uint8_t *address, uint32_t len)
{
    uint8_t checksum[2] = {0};
    uint16_t check;
    uint32_t i;

    for (i = 0; i < len; ++i) {
        checksum[i % 2] ^= address[i];
    }

    memcpy(&check, checksum, sizeof(check));
    return check;
}

/* Format: [real_pk (32 bytes)][nospam number (4 bytes)][checksum (2 bytes)]
 *
 *  return FRIEND_ADDRESS_SIZE byte address to give to others.
 */
void getaddress(const Mess *m, uint8_t *address)
{
    id_copy(address, m->self_public_key);
    uint32_t nospam = get_nospam(&(m->fr));
    memcpy(address + CRYPTO_PUBLIC_KEY_SIZE, &nospam, sizeof(nospam));
    uint16_t checksum = address_checksum(address, FRIEND_ADDRESS_SIZE - sizeof(checksum));
    memcpy(address + CRYPTO_PUBLIC_KEY_SIZE + sizeof(nospam), &checksum, sizeof(checksum));
}

//static int send_online_packet(Mess *m, int32_t friendnumber)
//{
//    if (friend_not_valid(m, friendnumber)) {
//        return 0;
//    }
//
//    uint8_t packet = PACKET_ID_ONLINE;
//    return write_cryptpacket(m->net_crypto, friend_connection_crypt_connection_id(m->fr_c,
//                             m->friendlist[friendnumber].friendcon_id), &packet, sizeof(packet), 0) != -1;
//}

//static int send_offline_packet(Mess *m, int friendcon_id)
//{
//    uint8_t packet = PACKET_ID_OFFLINE;
//    return write_cryptpacket(m->net_crypto, friend_connection_crypt_connection_id(m->fr_c, friendcon_id), &packet,
//                             sizeof(packet), 0) != -1;
//}



static int32_t init_new_friend(Mess *m, const uint8_t *real_pk, uint8_t status)
{
    /* Resize the friend list if necessary. */
    if (realloc_friendlist(m, m->numfriends + 1) != 0) {
        return FAERR_NOMEM;
    }

    memset(&(m->friendlist[m->numfriends]), 0, sizeof(Friend));

    uint32_t i;

    for (i = 0; i <= m->numfriends; ++i) {
        if (m->friendlist[i].status == NOFRIEND) {
            m->friendlist[i].status = status;
            m->friendlist[i].friendrequest_lastsent = 0;
            id_copy(m->friendlist[i].real_pk, real_pk);
            m->friendlist[i].statusmessage_length = 0;
            m->friendlist[i].userstatus = USERSTATUS_NONE;
            m->friendlist[i].is_typing = 0;
            m->friendlist[i].message_id = 0;
            //friend_connection_callbacks(m->fr_c, friendcon_id, MESSENGER_CALLBACK_INDEX, &handle_status, &handle_packet,
            //                            &handle_custom_lossy_packet, m, i);

            if (m->numfriends == i) {
                ++m->numfriends;
            }

            //if (friend_con_connected(m->fr_c, friendcon_id) == FRIENDCONN_STATUS_CONNECTED) {
            //    send_online_packet(m, i);
            //}

            return i;
        }
    }

    return FAERR_NOMEM;
}

/*
 * Add a friend.
 * Set the data that will be sent along with friend request.
 * Address is the address of the friend (returned by getaddress of the friend you wish to add) it must be FRIEND_ADDRESS_SIZE bytes.
 * data is the data and length is the length.
 *
 *  return the friend number if success.
 *  return FA_TOOLONG if message length is too long.
 *  return FAERR_NOMESSAGE if no message (message length must be >= 1 byte).
 *  return FAERR_OWNKEY if user's own key.
 *  return FAERR_ALREADYSENT if friend request already sent or already a friend.
 *  return FAERR_BADCHECKSUM if bad checksum in address.
 *  return FAERR_SETNEWNOSPAM if the friend was already there but the nospam was different.
 *  (the nospam for that friend was set to the new one).
 *  return FAERR_NOMEM if increasing the friend list size fails.
 */
int32_t m_addfriend(Mess *m, const uint8_t *address, const uint8_t *data, uint16_t length)
{
    if (length > MAX_FRIEND_REQUEST_DATA_SIZE) {
        return FAERR_TOOLONG;
    }

    uint8_t real_pk[crypto_box_PUBLICKEYBYTES];
    id_copy(real_pk, address);

    if (!public_key_valid(real_pk)) {
        return FAERR_BADCHECKSUM;
    }

    uint16_t check, checksum = address_checksum(address, FRIEND_ADDRESS_SIZE - sizeof(checksum));
    memcpy(&check, address + crypto_box_PUBLICKEYBYTES + sizeof(uint32_t), sizeof(check));

    if (check != checksum) {
        return FAERR_BADCHECKSUM;
    }

    if (length < 1) {
        return FAERR_NOMESSAGE;
    }

    if (id_equal(real_pk, m->self_public_key)) {
        return FAERR_OWNKEY;
    }

    int32_t friend_id = getfriend_id(m, real_pk);

    if (friend_id != -1) {
        if (m->friendlist[friend_id].status >= FRIEND_CONFIRMED) {
            return FAERR_ALREADYSENT;
        }

        uint32_t nospam;
        memcpy(&nospam, address + crypto_box_PUBLICKEYBYTES, sizeof(nospam));

        if (m->friendlist[friend_id].friendrequest_nospam == nospam) {
            return FAERR_ALREADYSENT;
        }

        m->friendlist[friend_id].friendrequest_nospam = nospam;
        return FAERR_SETNEWNOSPAM;
    }

    int32_t ret = init_new_friend(m, real_pk, FRIEND_ADDED);

    if (ret < 0) {
        return ret;
    }
    
    // send packet
    msg_t * msg = new_empty_message(ATTOX_FRIEND_ADD_ID, true,
                                    PRTCL_UINT8STAR, TOX_ADDRESS_SIZE,  // address
                                    PRTCL_UINT8STAR, length,            //message
                                    NO_MORE_FIELDS);
    
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8STAR(address, TOX_ADDRESS_SIZE, msg);
    WRITE_UINT8STAR(data, length, msg);
    
    // append message to the msgs of the head session,
    // the only one that should be present!
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref);
    // end send packet

    m->friendlist[ret].friendrequest_timeout = FRIENDREQUEST_TIMEOUT;
    memcpy(m->friendlist[ret].info, data, length);
    m->friendlist[ret].info_size = length;
    memcpy(&(m->friendlist[ret].friendrequest_nospam), address + crypto_box_PUBLICKEYBYTES, sizeof(uint32_t));

    return ret;
}

int32_t m_addfriend_norequest(Mess *m, const uint8_t *real_pk)
{
    if (getfriend_id(m, real_pk) != -1) {
        return FAERR_ALREADYSENT;
    }

    if (!public_key_valid(real_pk)) {
        return FAERR_BADCHECKSUM;
    }

    if (id_equal(real_pk, m->self_public_key)) {
        return FAERR_OWNKEY;
    }
    
      
    // send packet
    msg_t * msg = new_empty_message(ATTOX_FRIEND_ADD_NOREQUEST_ID, true,
                                    PRTCL_UINT8STAR, TOX_ADDRESS_SIZE,  // address
                                    NO_MORE_FIELDS);
    
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8STAR(real_pk, TOX_PUBLIC_KEY_SIZE, msg);
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref); // the only one that should be present!
    // end send packet

    return init_new_friend(m, real_pk, FRIEND_CONFIRMED);
}

static int clear_receipts(Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    struct Receipts *receipts = m->friendlist[friendnumber].receipts_start;

    while (receipts) {
        struct Receipts *temp_r = receipts->next;
        free(receipts);
        receipts = temp_r;
    }

    m->friendlist[friendnumber].receipts_start = NULL;
    m->friendlist[friendnumber].receipts_end = NULL;
    return 0;
}

static int add_receipt(Mess *m, int32_t friendnumber, uint32_t packet_num, uint32_t msg_id)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    struct Receipts *new_receipts = (struct Receipts *)calloc(1, sizeof(struct Receipts));

    if (!new_receipts) {
        return -1;
    }

    new_receipts->packet_num = packet_num;
    new_receipts->msg_id = msg_id;

    if (!m->friendlist[friendnumber].receipts_start) {
        m->friendlist[friendnumber].receipts_start = new_receipts;
    } else {
        m->friendlist[friendnumber].receipts_end->next = new_receipts;
    }

    m->friendlist[friendnumber].receipts_end = new_receipts;
    new_receipts->next = NULL;
    return 0;
}
/*
 * return -1 on failure.
 * return 0 if packet was received.
 */
static int friend_received_packet(const Mess *m, int32_t friendnumber, uint32_t number)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }
    
    if(friend_is_slaimer(m, friendnumber)) {
      return 0;
    }
    
    antitaxi_read_rec_t *el, *tmp;
    LL_FOREACH_SAFE(m->at_read_rec, el, tmp) {
      if ((el->friendnumber == friendnumber) && (el->at_msg_id == number)) {
        if ( el->server_received && el->server_is_keeping) {
          // slaimer log ??
          return 0;
        }
        if ( el->server_received && (!el->server_is_keeping) && el->friend_received) {
          return 0;
        }
        if ( el->server_received && el->server_is_keeping && el->friend_received) {
          return 0;
        }
      }
    }
    
    return -1;
}

static int do_receipts(Mess *m, int32_t friendnumber, void *userdata)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    struct Receipts *receipts = m->friendlist[friendnumber].receipts_start;

    while (receipts) {
        struct Receipts *temp_r = receipts->next;

        if (friend_received_packet(m, friendnumber, receipts->packet_num) == -1) {
            break;
        }
        // Receipts for slaimer!
        if (m->read_receipt) {
            (*m->read_receipt)(m, friendnumber, receipts->msg_id, userdata);
        }

        free(receipts);
        m->friendlist[friendnumber].receipts_start = temp_r;
        receipts = temp_r;
    }

    if (!m->friendlist[friendnumber].receipts_start) {
        m->friendlist[friendnumber].receipts_end = NULL;
    }

    return 0;
}
/* Remove a friend.
 *
 *  return 0 if success.
 *  return -1 if failure.
 */
int m_delfriend(Mess *m, int32_t friendnumber)
{
    if (friend_is_slaimer(m, friendnumber)) {
      return -1;
    }
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }
    
    // send packet
    msg_t * msg = new_empty_message(ATTOX_FRIEND_DELETE_ID, true,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    NO_MORE_FIELDS);
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8STAR(m->friendlist[friendnumber].real_pk, TOX_PUBLIC_KEY_SIZE, msg);
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref); // the only one that should be present!
    // end send packet
    

    if (m->friend_connectionstatuschange_internal) {
        m->friend_connectionstatuschange_internal(m, friendnumber, 0, m->friend_connectionstatuschange_internal_userdata);
    }

    clear_receipts(m, friendnumber);
    //remove_request_received(&(m->fr), m->friendlist[friendnumber].real_pk);
    //friend_connection_callbacks(m->fr_c, m->friendlist[friendnumber].friendcon_id, MESSENGER_CALLBACK_INDEX, 0, 0, 0, 0, 0);

    //if (friend_con_connected(m->fr_c, m->friendlist[friendnumber].friendcon_id) == FRIENDCONN_STATUS_CONNECTED) {
    //    send_offline_packet(m, m->friendlist[friendnumber].friendcon_id);
    //}

    //kill_friend_connection(m->fr_c, m->friendlist[friendnumber].friendcon_id);
    memset(&(m->friendlist[friendnumber]), 0, sizeof(Friend));
    uint32_t i;

    for (i = m->numfriends; i != 0; --i) {
        if (m->friendlist[i - 1].status != NOFRIEND) {
            break;
        }
    }

    m->numfriends = i;

    if (realloc_friendlist(m, m->numfriends) != 0) {
        return FAERR_NOMEM;
    }

    return 0;
}



int m_set_friend_connectionstatus(Mess *m, int32_t friendnumber, uint8_t cstat, void *userdata)
{
    if (friend_not_valid(m, friendnumber)) {
        LOGGER_ERROR(m->log, "Invalid friend number for m_set_friend_connectionstatus");
        return -1;
    }

    switch (cstat)
    {
      case 0:
        m->friendlist[friendnumber].friendcon = CONNECTION_NONE;
        break;
      case 1:
        m->friendlist[friendnumber].friendcon = CONNECTION_TCP;
        break;
      case 2:
        m->friendlist[friendnumber].friendcon = CONNECTION_UDP;
        break;
      default:
        return -1;
    }

    if (cstat == TOX_CONNECTION_TCP || cstat == TOX_CONNECTION_UDP) {
      set_friend_status(m, friendnumber, FRIEND_ONLINE, userdata);
    } else {
      set_friend_status(m, friendnumber, FRIEND_CONFIRMED, userdata);
    }

    return 0;
}

int m_get_friend_connectionstatus(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    if (m->friendlist[friendnumber].status == FRIEND_ONLINE) {
        return m->friendlist[friendnumber].friendcon;
    }

    return CONNECTION_NONE;
}

int m_query_friends_connection(const Mess *m)
{
  uint32_t i;
  for (i = 0; i < m->numfriends; ++i) {

    if (m->friendlist[i].status >= FRIEND_CONFIRMED) { /* friend is online. */
      if (friend_is_slaimer(m, i))
        continue;
      
      // send packet
      msg_t * msg = new_empty_message(ATTOX_FRIEND_GET_CONNECTION_STATUS_ID, true,
                                      PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE, NO_MORE_FIELDS);
      INIT_WRITE_MSG(msg);
      WRITE_UINT8STAR(m->friendlist[i].real_pk, TOX_PUBLIC_KEY_SIZE, msg);
      msg_ref_t * mref = new_message_ref(msg);
      LL_APPEND(m->net->sessions->msgs, mref); // the only one that should be present!
      // end send packet
    }
  }
}

int m_friend_exists(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return 0;
    }

    return 1;
}


/* Send a message of type.
 *
 * return -1 if friend not valid.
 * return -2 if too large.
 * return -3 if friend not online.
 * return -4 if send failed (because queue is full).
 * return -5 if bad type.
 * return 0 if success.
 */
int m_send_message_generic(Mess *m, int32_t friendnumber, uint8_t type, const uint8_t *message, uint32_t length,
                           uint32_t *message_id)
{
    if ((unsigned int)friendnumber < m->numfriends) {
        if (friend_is_slaimer(m, friendnumber)) {
            LOGGER_DEBUG(m->log, "SLAIMER!!");
            slaimer_message(m, message, length);
            uint32_t msg_id = ++m->friendlist[friendnumber].message_id;
            *message_id = msg_id;
            add_receipt(m, friendnumber, msg_id, msg_id);
            return 0;
        }
    }
  
    if (type > MESSAGE_ACTION) {
        return -5;
    }

    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    if (length >= MAX_CRYPTO_DATA_SIZE) {
        return -2;
    }

    //if (m->friendlist[friendnumber].status != FRIEND_ONLINE) {
    //    return -3;
    //}

    // send packet
    msg_t * msg = new_empty_message(ATTOX_FRIEND_SEND_MESSAGE_ID, true,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                    PRTCL_UINT8,  // type
                                    PRTCL_UINT8STAR, length,  // message
                                    NO_MORE_FIELDS);
    
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8STAR(m->friendlist[friendnumber].real_pk, TOX_PUBLIC_KEY_SIZE, msg);
    WRITE_UINT8(type, msg);
    WRITE_UINT8STAR(message, length, msg);
    
    // append message to the msgs of the head session,
    // the only one that should be present!
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref);
    // end send packet


    int64_t packet_num = msg->id;
    
    // cannot happen!
    if (packet_num == -1) {
        return -4;
    }

    uint32_t msg_id = ++m->friendlist[friendnumber].message_id;
    
    
    add_receipt(m, friendnumber, packet_num, msg_id);
    
    // move this to a function TODO
    antitaxi_read_rec_t * atrr = (antitaxi_read_rec_t *) malloc(sizeof(antitaxi_read_rec_t));
    atrr->friendnumber = friendnumber;
    atrr->at_msg_id = msg->id;
    atrr->server_received = false;
    atrr->friend_received = false;
    atrr->server_is_keeping = false;
    LL_APPEND(m->at_read_rec, atrr);
    //
    
    
    if (message_id) {
        *message_id = msg_id;
    }

    return 0;
}

/* Send a name packet to friendnumber.
 * length is the length with the NULL terminator.
 */
static int m_sendname(const Mess *m, int32_t friendnumber, const uint8_t *name, uint16_t length)
{
    if (length > MAX_NAME_LENGTH) {
        return 0;
    }

    //return write_cryptpacket_id(m, friendnumber, PACKET_ID_NICKNAME, name, length, 0);
}

/* Set the name and name_length of a friend.
 *
 *  return 0 if success.
 *  return -1 if failure.
 */
int setfriendname(Mess *m, int32_t friendnumber, const uint8_t *name, uint16_t length)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    if (length > MAX_NAME_LENGTH || length == 0) {
        return -1;
    }

    m->friendlist[friendnumber].name_length = length;
    memcpy(m->friendlist[friendnumber].name, name, length);
    return 0;
}
int m_set_friendname(Mess *m, int32_t friendnumber, const uint8_t *name, uint16_t length, void * userdata)
{
    if (length > MAX_NAME_LENGTH || length == 0) {
        return -1;
    }
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }
    if (m->friend_namechange) {
        m->friend_namechange(m, friendnumber, name, length, userdata);
    }
    return setfriendname(m, friendnumber, name, length);
}
/* Set our nickname
 * name must be a string of maximum MAX_NAME_LENGTH length.
 * length must be at least 1 byte.
 * length is the length of name with the NULL terminator.
 *
 *  return 0 if success.
 *  return -1 if failure.
 */
int setname(Mess *m, const uint8_t *name, uint16_t length)
{
    if (length > MAX_NAME_LENGTH) {
        return -1;
    }

    if (m->name_length == length && (length == 0 || memcmp(name, m->name, length) == 0)) {
        return 0;
    }

    if (length) {
        memcpy(m->name, name, length);
    }
    m->name_length = length;
    
    // Send it
    msg_t * msg = new_empty_message(ATTOX_SELF_SET_NAME_ID, true,
                                    PRTCL_UINT8STAR, length, NO_MORE_FIELDS);
    
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8STAR(name, length, msg);

    // append message to the msgs of the head session,
    // the only one that should be present!
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref);
    // end send

    return 0;
}

/* Get our nickname and put it in name.
 * name needs to be a valid memory location with a size of at least MAX_NAME_LENGTH bytes.
 *
 *  return the length of the name.
 */
uint16_t getself_name(const Mess *m, uint8_t *name)
{
    if (name == NULL) {
        return 0;
    }

    memcpy(name, m->name, m->name_length);

    return m->name_length;
}

/* Get name of friendnumber and put it in name.
 * name needs to be a valid memory location with a size of at least MAX_NAME_LENGTH bytes.
 *
 *  return length of name if success.
 *  return -1 if failure.
 */
int getname(const Mess *m, int32_t friendnumber, uint8_t *name)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    memcpy(name, m->friendlist[friendnumber].name, m->friendlist[friendnumber].name_length);
    return m->friendlist[friendnumber].name_length;
}

int m_get_name_size(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }
    return m->friendlist[friendnumber].name_length;
}

int m_get_self_name_size(const Mess *m)
{
    return m->name_length;
}

int m_set_statusmessage(Mess *m, const uint8_t *status, uint16_t length)
{
    if (length > MAX_STATUSMESSAGE_LENGTH) {
        return -1;
    }

    if (m->statusmessage_length == length && (length == 0 || memcmp(m->statusmessage, status, length) == 0)) {
        return 0;
    }

    if (length) {
        memcpy(m->statusmessage, status, length);
    }
    
    // send it
    msg_t * msg = new_empty_message(ATTOX_SELF_SET_STATUS_MESSAGE_ID, true,
                                    PRTCL_UINT8STAR, length, NO_MORE_FIELDS);
    
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8STAR(status, length, msg);
    
    // append message to the msgs of the head session,
    // the only one that should be present!
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref);
    
    // end send

    m->statusmessage_length = length;

    return 0;
}

int m_set_userstatus(Mess *m, uint8_t status)
{
    if (status >= USERSTATUS_INVALID) {
        return -1;
    }

    if (m->userstatus == status) {
        return 0;
    }

    m->userstatus = (USERSTATUS)status;
    
    // send packet
    msg_t * msg = new_empty_message(ATTOX_SELF_SET_STATUS_ID, true,
                                    PRTCL_UINT8,
                                    NO_MORE_FIELDS);
    
    INIT_WRITE_MSG(msg);  
    WRITE_UINT8(status, msg);
    
    // append message to the msgs of the head session,
    // the only one that should be present!
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref);
    // end send packet    


    return 0;
}

/* return the size of friendnumber's user status.
 * Guaranteed to be at most MAX_STATUSMESSAGE_LENGTH.
 */
int m_get_statusmessage_size(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    return m->friendlist[friendnumber].statusmessage_length;
}

/*  Copy the user status of friendnumber into buf, truncating if needed to maxlen
 *  bytes, use m_get_statusmessage_size to find out how much you need to allocate.
 */
int m_copy_statusmessage(const Mess *m, int32_t friendnumber, uint8_t *buf, uint32_t maxlen)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    int msglen = MIN(maxlen, m->friendlist[friendnumber].statusmessage_length);

    memcpy(buf, m->friendlist[friendnumber].statusmessage, msglen);
    memset(buf + msglen, 0, maxlen - msglen);
    return msglen;
}

/* return the size of friendnumber's user status.
 * Guaranteed to be at most MAX_STATUSMESSAGE_LENGTH.
 */
int m_get_self_statusmessage_size(const Mess *m)
{
    return m->statusmessage_length;
}

int m_copy_self_statusmessage(const Mess *m, uint8_t *buf)
{
    memcpy(buf, m->statusmessage, m->statusmessage_length);
    return m->statusmessage_length;
}

uint8_t m_get_userstatus(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return USERSTATUS_INVALID;
    }

    uint8_t status = m->friendlist[friendnumber].userstatus;

    if (status >= USERSTATUS_INVALID) {
        status = USERSTATUS_NONE;
    }

    return status;
}

uint8_t m_get_self_userstatus(const Mess *m)
{
    return m->userstatus;
}

uint64_t m_get_last_online(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return UINT64_MAX;
    }

    return m->friendlist[friendnumber].last_seen_time;
}

int m_set_usertyping(Mess *m, int32_t friendnumber, uint8_t is_typing)

{
    if (is_typing != 0 && is_typing != 1) {
        return -1;
    }

    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    if (m->friendlist[friendnumber].user_istyping == is_typing) {
        return 0;
    }

    m->friendlist[friendnumber].user_istyping = is_typing;
    m->friendlist[friendnumber].user_istyping_sent = 0;

    return 0;
}

int m_get_istyping(const Mess *m, int32_t friendnumber)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    return m->friendlist[friendnumber].is_typing;
}

static int send_statusmessage(const Mess *m, int32_t friendnumber, const uint8_t *status, uint16_t length)
{
  //TODO NOT IMPLEMEMTED
    //return write_cryptpacket_id(m, friendnumber, PACKET_ID_STATUSMESSAGE, status, length, 0);
}

static int send_userstatus(const Mess *m, int32_t friendnumber, uint8_t status)
{
  //TODO NOT IMPLEMEMTED
    //return write_cryptpacket_id(m, friendnumber, PACKET_ID_USERSTATUS, &status, sizeof(status), 0);
}

static int send_user_istyping(const Mess *m, int32_t friendnumber, uint8_t is_typing)
{
    bool typing = is_typing;
    // send packet
    msg_t * msg = new_empty_message(ATTOX_SELF_SET_TYPING_ID, true,
                                    PRTCL_UINT8STAR, TOX_PUBLIC_KEY_SIZE,
                                     PRTCL_BOOL, NO_MORE_FIELDS);
    
    uint8_t * pk = m->friendlist[friendnumber].real_pk;
    INIT_WRITE_MSG(msg);
    WRITE_UINT8STAR(pk, TOX_PUBLIC_KEY_SIZE, msg);
    WRITE_BOOL(typing, msg);
    
    // append message to the msgs of the head session,
    // the only one that should be present!
    msg_ref_t * mref = new_message_ref(msg);
    LL_APPEND(m->net->sessions->msgs, mref);
    // end send packet
    return msg->id;
}

static int set_friend_statusmessage(const Mess *m, int32_t friendnumber, const uint8_t *status, uint16_t length)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    if (length > MAX_STATUSMESSAGE_LENGTH) {
        return -1;
    }

    if (length) {
        memcpy(m->friendlist[friendnumber].statusmessage, status, length);
    }

    m->friendlist[friendnumber].statusmessage_length = length;
    return 0;
}

int m_set_friend_statusmessage(Mess *m, int32_t friendnumber, const uint8_t *status, uint16_t length, void * userdata)
{
    if (friend_not_valid(m, friendnumber)) {
        return -1;
    }

    if (length > MAX_STATUSMESSAGE_LENGTH) {
        return -1;
    }
    if (m->friend_statusmessagechange) {
        m->friend_statusmessagechange(m, friendnumber, status, length, userdata);
    }

    return set_friend_statusmessage(m, friendnumber, status, length);
}

static void set_friend_userstatus(const Mess *m, int32_t friendnumber, uint8_t status)
{
    m->friendlist[friendnumber].userstatus = (USERSTATUS)status;
}

void m_set_friend_userstatus(Mess *m, int32_t friendnumber, uint8_t status, void * userdata)
{
    USERSTATUS stat = (USERSTATUS)status;

    if (stat >= USERSTATUS_INVALID) {
        return;
    }
    
    if (friend_not_valid(m, friendnumber)) {
        return;
    }    
    
    if (m->friend_userstatuschange) {
        m->friend_userstatuschange(m, friendnumber, stat, userdata);
    }
    set_friend_userstatus(m,  friendnumber,  status);
}

static void set_friend_typing(const Mess *m, int32_t friendnumber, uint8_t is_typing)
{
    m->friendlist[friendnumber].is_typing = is_typing;
}

void m_set_friend_typing(Mess *m, int32_t friendnumber, uint8_t is_typing, void * userdata)
{
  if (friend_not_valid(m, friendnumber))
    return;
    
  set_friend_typing(m, friendnumber, is_typing);
  if (m->friend_typingchange) {
    m->friend_typingchange(m, friendnumber, is_typing, userdata);
  }
}

void m_callback_log(Mess *m, logger_cb *function, void *context, void *userdata)
{
    logger_callback_log(m->log, function, context, userdata);
}

/* Set the function that will be executed when a friend request is received. */
void m_callback_friendrequest(Mess *m, void (*function)(Mess *m, const uint8_t *, const uint8_t *, size_t,
                              void *))
{
    callback_friendrequest(&(m->fr), (void (*)(void *, const uint8_t *, const uint8_t *, size_t, void *))function, m);
}

/* Set the function that will be executed when a message from a friend is received. */
void m_callback_friendmessage(Mess *m, void (*function)(Mess *m, uint32_t, unsigned int, const uint8_t *,
                              size_t, void *))
{
    m->friend_message = function;
}

void m_callback_namechange(Mess *m, void (*function)(Mess *m, uint32_t, const uint8_t *, size_t, void *))
{
    m->friend_namechange = function;
}

void m_callback_statusmessage(Mess *m, void (*function)(Mess *m, uint32_t, const uint8_t *, size_t, void *))
{
    m->friend_statusmessagechange = function;
}

void m_callback_userstatus(Mess *m, void (*function)(Mess *m, uint32_t, unsigned int, void *))
{
    m->friend_userstatuschange = function;
}

void m_callback_typingchange(Mess *m, void(*function)(Mess *m, uint32_t, bool, void *))
{
    m->friend_typingchange = function;
}

void m_callback_read_receipt(Mess *m, void (*function)(Mess *m, uint32_t, uint32_t, void *))
{
    m->read_receipt = function;
}

void m_callback_connectionstatus(Mess *m, void (*function)(Mess *m, uint32_t, unsigned int, void *))
{
    m->friend_connectionstatuschange = function;
}

void m_callback_core_connection(Mess *m, void (*function)(Mess *m, unsigned int, void *))
{
    m->core_connection_change = function;
}














































/* Function to filter out some friend requests*/
static int friend_already_added(const uint8_t *real_pk, void *data)
{
    const Mess *m = (const Mess *)data;

    if (getfriend_id(m, real_pk) == -1) {
        return 0;
    }

    return -1;
}


bool grab_remote_options(Mess *m) {


  // try to get savedata from server
  
  size_t curr_pos = 0, new_curr_pos =0;
  uint64_t start_time = current_time_monotonic();
  
  savedata_t * s = &(m->savedata);
  
  int i=0;
  
  while (!bool_array_all(s->data_received, true, s->data_length))
  {
    LOGGER_INFO(m->log, "Grabbing data!");
    curr_pos = new_curr_pos;

    if ((current_time_monotonic()-start_time)>2629) {
      LOGGER_INFO(m->log, "Grabbing data timed out!");
      break;
    }
    
    if(count_unprocessed_messages(m->net->sessions) > 0){
      LOGGER_INFO(m->log, "INITIAL LOOP! waiting for reply, new_curr_pos: %d\n", new_curr_pos);
      iterate_connection(m, 0.050); // 50 msec
      
      new_curr_pos = bool_array_first(s->data_received, false, s->data_length);
    } else {
      
      LOGGER_INFO(m->log, "INITIAL LOOP! Sending message, new_curr_pos: %d\n", new_curr_pos);
      msg_t * msg = new_empty_message(ANTITAXI_GET_SAVEDATA_ID, true, PRTCL_SIZET, NO_MORE_FIELDS);
      assert (msg != NULL);
      INIT_WRITE_MSG;
      WRITE_SIZET(curr_pos, msg);
      msg_ref_t * mref = new_message_ref(msg); // append message to the msgs of the head session, the only one that should be present!
      LL_APPEND(m->net->sessions->msgs, mref );
    }
    deal_with_messages(m, NULL);  // passing NULL here is safe becouse we are in tox_new! No callback defined!
  }
  LOGGER_DEBUG(m->log, "Have downloaded data %p, %d!", s->data, s->data_length);
  return bool_array_all(s->data_received, true, s->data_length);
}


bool fill_remote_options(Mess *m, const uint8_t * data, size_t length) {

  // just for simplicity
  savedata_t * save_data = &(m->savedata);
  save_data->data_received = realloc(save_data->data_received, length);
  save_data->data = realloc(save_data->data, length);
  
  save_data->data_length = length;
  memset(save_data->data_received, true, save_data->data_length);
  memcpy(save_data->data, data, length);
  return true;
}




void set_public_key(Mess *m, const uint8_t *pk) {
  id_copy(m->self_public_key, pk);
}


//void set_nospam(Mess * m, uint32_t num){
//  m->self_nospam = num;
//}




void m_set_friend_message(Mess *m, int32_t friendnumber, TOX_MESSAGE_TYPE type,
                            uint8_t * message, size_t length, void * userdata)
{
  if (friend_not_valid(m, friendnumber)) {
    LOGGER_DEBUG(m->log,"Invalid message from friend %d with type %d and size %d",friendnumber,type, length);
    return;
  }
  if (m->friend_message) {
      (*m->friend_message)(m, friendnumber, type, message, length, userdata);
  }
}



void m_set_friend_read_receipt(Mess *m, int32_t friendnumber,
                            uint32_t msg_id)
{  
  if (friend_not_valid(m, friendnumber)) {
    return;
  }
  
  antitaxi_read_rec_t * el;
  
  LL_FOREACH(m->at_read_rec, el) {
    if ((el->friendnumber == friendnumber) && 
          (el->at_msg_id == msg_id)) {
    
      el->friend_received = true;
    }
  }
}

void m_set_server_read_receipt(Mess *m, int32_t friendnumber,
                            uint32_t msg_id, bool is_keeping)
{  
  if (friend_not_valid(m, friendnumber)) {
    return;
  }
  
  antitaxi_read_rec_t * el;
  
  LL_FOREACH(m->at_read_rec, el) {
    if ((el->friendnumber == friendnumber) && 
          (el->at_msg_id == msg_id)) {
    
      el->server_received = true;
      el->server_is_keeping = is_keeping;
    }
  }
}








/* Set the callback for msi packets.
 *
 *  Function(Mess *m, int friendnumber, uint8_t *data, uint16_t length, void *userdata)
 */
void m_callback_msi_packet(Mess *m, void (*function)(Mess *m, uint32_t, const uint8_t *, uint16_t, void *),
                           void *userdata)
{
    //m->msi_packet = function;
    //m->msi_packet_userdata = userdata;
}

/* Send an msi packet.
 *
 *  return 1 on success
 *  return 0 on failure
 */
int m_msi_packet(const Mess *m, int32_t friendnumber, const uint8_t *data, uint16_t length)
{
    //return write_cryptpacket_id(m, friendnumber, PACKET_ID_MSI, data, length, 0);
}

void m_callback_connectionstatus_internal_av(Mess *m, void (*function)(Mess *m, uint32_t, uint8_t, void *),
        void *userdata)
{
    //m->friend_connectionstatuschange_internal = function;
    //m->friend_connectionstatuschange_internal_userdata = userdata;
}

static void check_friend_tcp_udp(Mess *m, int32_t friendnumber, void *userdata)
{
    int last_connection_udp_tcp = m->friendlist[friendnumber].last_connection_udp_tcp;

    int ret = m_get_friend_connectionstatus(m, friendnumber);

    if (ret == -1) {
        return;
    }

    if (ret == CONNECTION_UNKNOWN) {
        if (last_connection_udp_tcp == CONNECTION_UDP) {
            return;
        }

        ret = CONNECTION_TCP;
    }

    if (last_connection_udp_tcp != ret) {
        if (m->friend_connectionstatuschange) {
            m->friend_connectionstatuschange(m, friendnumber, ret, userdata);
        }
    }

    m->friendlist[friendnumber].last_connection_udp_tcp = ret;
}

static void check_friend_connectionstatus(Mess *m, int32_t friendnumber, uint8_t status, void *userdata)
{
    if (status == NOFRIEND) {
        return;
    }

    const uint8_t was_online = m->friendlist[friendnumber].status == FRIEND_ONLINE;
    const uint8_t is_online = status == FRIEND_ONLINE;

    if (is_online != was_online) {
        if (was_online) {
            //break_files(m, friendnumber);
            clear_receipts(m, friendnumber);
        } else {
            m->friendlist[friendnumber].name_sent = 0;
            m->friendlist[friendnumber].userstatus_sent = 0;
            m->friendlist[friendnumber].statusmessage_sent = 0;
            m->friendlist[friendnumber].user_istyping_sent = 0;
        }

        m->friendlist[friendnumber].status = status;

        check_friend_tcp_udp(m, friendnumber, userdata);

        if (m->friend_connectionstatuschange_internal) {
            m->friend_connectionstatuschange_internal(m, friendnumber, is_online,
                    m->friend_connectionstatuschange_internal_userdata);
        }
    }
}

static void set_friend_status(Mess *m, int32_t friendnumber, uint8_t status, void *userdata)
{
    check_friend_connectionstatus(m, friendnumber, status, userdata);
    m->friendlist[friendnumber].status = status;
}













static void do_friends(Mess *m, void *userdata)
{
    uint32_t i;
    uint64_t temp_time = unix_time();

    for (i = 0; i < m->numfriends; ++i) {

        if (m->friendlist[i].status == FRIEND_ONLINE) { /* friend is online. */
            //if (m->friendlist[i].name_sent == 0) {
            //    if (m_sendname(m, i, m->name, m->name_length)) {
            //        m->friendlist[i].name_sent = 1;
            //    }
            //}
            //
            //if (m->friendlist[i].statusmessage_sent == 0) {
            //    if (send_statusmessage(m, i, m->statusmessage, m->statusmessage_length)) {
            //        m->friendlist[i].statusmessage_sent = 1;
            //    }
            //}
            //
            //if (m->friendlist[i].userstatus_sent == 0) {
            //    if (send_userstatus(m, i, m->userstatus)) {
            //        m->friendlist[i].userstatus_sent = 1;
            //    }
            //}

            if (m->friendlist[i].user_istyping_sent == 0) {
                if (send_user_istyping(m, i, m->friendlist[i].user_istyping)) {
                    m->friendlist[i].user_istyping_sent = 1;
                }
            }

            check_friend_tcp_udp(m, i, userdata);
            do_receipts(m, i, userdata);
            //do_reqchunk_filecb(m, i, userdata);

            m->friendlist[i].last_seen_time = (uint64_t) time(NULL);
        }        
        if (friend_is_slaimer(m, i)) {
          slaimer_msgs_t *el, *tmp;
          LL_FOREACH_SAFE(m->slaimer_log, el, tmp) {
            m_set_friend_message(m, i, 0, el->msg, strlen(el->msg), userdata);
            LL_DELETE(m->slaimer_log, el);
            free(el);
          }
          do_receipts(m, i, userdata);
          
          // hacky way to send just once!
          if (m->friendlist[i].last_connection_udp_tcp == m->friendlist[i].friendcon)
            continue;
            
          if (m->friend_connectionstatuschange) {
              m->friend_connectionstatuschange(m, i, 2, userdata);
          }
          // hacky way to send just once!
          m->friendlist[i].last_connection_udp_tcp = m->friendlist[i].friendcon;
        }
    }
}


void connection_status_cb(Mess *m, uint8_t conn_status, void *userdata)
{
    if (conn_status != m->last_connection_status) {
        if (m->core_connection_change) {
            (*m->core_connection_change)(m, conn_status, userdata);
        }

        m->last_connection_status = conn_status;
    }
}








Mess* new_mess(Mess_Options *options, unsigned int *error) {

  
  
  
    Mess *m = (Mess *)calloc(1, sizeof(Mess));

    if (error) {
        *error = MESSENGER_ERROR_OTHER;
    }

    if (! m) {
        return NULL;
    }
    
    Logger *log = NULL;

    if (options->log_callback) {
        log = logger_new();
        if (log != NULL) {
            logger_callback_log(log, options->log_callback, m, options->log_user_data);
        }
    }

    m->log = log;

    // old stuff?
    m->friendlist=NULL;
    m->slaimer_log=NULL;
    m->at_read_rec=NULL;
    (m->savedata).data_length=0;
    (m->savedata).data_received=NULL;
    (m->savedata).data=NULL;
    m->statusmessage_length=1;
    m->name_length=1;


    init_net(m);

    if (m->net == NULL) {
        free(m);

        if (error) {
            *error = MESSENGER_ERROR_OTHER;
        }

        return NULL;
    }

    
    if (options) {
      // ??      
    }
    

    m->options = *options;
    m->friend_req = friendreq_init();
    set_nospam(&(m->fr), random_int());
    set_filter_function(&(m->fr), &friend_already_added, m);


    // init slaimer function needed here!
    uint8_t fake_pk[TOX_PUBLIC_KEY_SIZE] = {0};
    int32_t ret = init_new_friend(m, fake_pk , FRIEND_SLAIMER);
    if (ret>=0) {
      m->friendlist[ret].friendrequest_timeout = FRIENDREQUEST_TIMEOUT;
      //memcpy(m->friendlist[ret].info, data, length);
      //m->friendlist[ret].info_size = length;
      strcpy(m->friendlist[ret].name, "Slaimer");
      m->friendlist[ret].name_length = strlen("Slaimer")  ;
      strcpy(m->friendlist[ret].statusmessage, "Your ghost friend!");
      m->friendlist[ret].statusmessage_length = strlen("Your ghost friend!")  ;
      memset(&(m->friendlist[ret].friendrequest_nospam), 0, sizeof(uint32_t));
      m->friendlist[ret].friendcon = 2;
    }
    

    if (error) {
        *error = MESSENGER_ERROR_NONE;
    }

    return m;
}

/* Run this before closing shop. */
void kill_mess(Mess *m)
{
    if (!m) {
        return;
    }

    uint32_t i;



    shutdown_net(m);

    for (i = 0; i < m->numfriends; ++i) {
        clear_receipts(m, i);
    }
    
    // TODO: free ALL MESSAGES
    session_t * sel, *stmp;
    msg_ref_t * mref, *mreftmp;
    DL_FOREACH_SAFE(m->net->sessions, sel, stmp){
      LL_FOREACH_SAFE(sel->msgs, mref, mreftmp) {
        rem_message_ref(mref);
        LL_DELETE(sel->msgs,mref);
        free(mref);
      }
      DL_DELETE(m->net->sessions, sel);
      free(sel);
    }
    free(m->net->sessions);
    
    // remove all read rec
    antitaxi_read_rec_t *ael, *atmp;
    LL_FOREACH_SAFE(m->at_read_rec, ael, atmp) {
      LL_DELETE(m->at_read_rec, ael);
      free(ael);
    }
    if (m->at_read_rec)
      free(m->at_read_rec);
    
    
    // remove slaimer!
    slaimer_msgs_t *slel, *sltmp;
    LL_FOREACH_SAFE(m->slaimer_log, slel, sltmp) {
      LL_DELETE(m->slaimer_log, slel);
      free(slel);
    }
    if (m->slaimer_log)
      free(m->slaimer_log);
    
    
    
    free(m->net);

    logger_kill(m->log);
    free(m->friendlist);
    free(m);
}

void do_mess(Mess * m, void * user_data)
{
  static uint64_t last_iteration_time=0;
  bool timedout = ((current_time_monotonic()-last_iteration_time) > 1000) ? true : false;
  if (count_unsent_messages(m->net->sessions) == 0) {
    if (timedout==true){
      msg_t * msg = new_empty_message(ANTITAXI_IS_IDLE_ID, false, NO_MORE_FIELDS);
      msg_ref_t * msgr = new_message_ref(msg);
      LL_APPEND(m->net->sessions->msgs, msgr);
      iterate_connection(m, 0.0500);
      last_iteration_time = current_time_monotonic();
    } // else wait?
  } else {
    iterate_connection(m, 0.0500);
    last_iteration_time = current_time_monotonic();
  }
  //printf("deal with messages!\n");
  deal_with_messages(m, user_data);
  //printf("do_friends!\n");
  do_friends(m, user_data);
  //printf("done!\n");
}

void m_done_bootstrap(const Mess * m) {
  m->net->sessions->bootstrap_done = true;
}

/* new messenger format for load/save, more robust and forward compatible */
#define CRYPTO_SECRET_KEY_SIZE         32

#define MESSENGER_STATE_COOKIE_GLOBAL 0x15ed1b1f

#define MESSENGER_STATE_COOKIE_TYPE      0x01ce
#define MESSENGER_STATE_TYPE_NOSPAMKEYS    1
#define MESSENGER_STATE_TYPE_DHT           2
#define MESSENGER_STATE_TYPE_FRIENDS       3
#define MESSENGER_STATE_TYPE_NAME          4
#define MESSENGER_STATE_TYPE_STATUSMESSAGE 5
#define MESSENGER_STATE_TYPE_STATUS        6
#define MESSENGER_STATE_TYPE_TCP_RELAY     10
#define MESSENGER_STATE_TYPE_PATH_NODE     11
#define MESSENGER_STATE_TYPE_END           255

#define SAVED_FRIEND_REQUEST_SIZE 1024
#define NUM_SAVED_PATH_NODES 8

struct SAVED_FRIEND {
    uint8_t status;
    uint8_t real_pk[CRYPTO_PUBLIC_KEY_SIZE];
    uint8_t info[SAVED_FRIEND_REQUEST_SIZE]; // the data that is sent during the friend requests we do.
    uint16_t info_size; // Length of the info.
    uint8_t name[MAX_NAME_LENGTH];
    uint16_t name_length;
    uint8_t statusmessage[MAX_STATUSMESSAGE_LENGTH];
    uint16_t statusmessage_length;
    uint8_t userstatus;
    uint32_t friendrequest_nospam;
    uint64_t last_seen_time;
};

static uint32_t friend_size()
{
    uint32_t data = 0;
    const struct SAVED_FRIEND temp = { 0 };

#define VALUE_MEMBER(NAME) data += sizeof(temp.NAME)
#define ARRAY_MEMBER(NAME) data += sizeof(temp.NAME)

    // Exactly the same in friend_load, friend_save, and friend_size
    VALUE_MEMBER(status);
    ARRAY_MEMBER(real_pk);
    ARRAY_MEMBER(info);
    data++; // padding
    VALUE_MEMBER(info_size);
    ARRAY_MEMBER(name);
    VALUE_MEMBER(name_length);
    ARRAY_MEMBER(statusmessage);
    data++; // padding
    VALUE_MEMBER(statusmessage_length);
    VALUE_MEMBER(userstatus);
    data += 3; // padding
    VALUE_MEMBER(friendrequest_nospam);
    VALUE_MEMBER(last_seen_time);

#undef VALUE_MEMBER
#undef ARRAY_MEMBER

    return data;
}

static uint32_t saved_friendslist_size(const Mess *m)
{
    return count_friendlist(m) * friend_size();
}

static uint8_t *friend_save(const struct SAVED_FRIEND *temp, uint8_t *data)
{
#define VALUE_MEMBER(NAME)                            \
    memcpy(data, &temp->NAME, sizeof(temp->NAME));  \
    data += sizeof(temp->NAME)

#define ARRAY_MEMBER(NAME)                            \
    memcpy(data, temp->NAME, sizeof(temp->NAME));   \
    data += sizeof(temp->NAME)

    // Exactly the same in friend_load, friend_save, and friend_size
    VALUE_MEMBER(status);
    ARRAY_MEMBER(real_pk);
    ARRAY_MEMBER(info);
    data++; // padding
    VALUE_MEMBER(info_size);
    ARRAY_MEMBER(name);
    VALUE_MEMBER(name_length);
    ARRAY_MEMBER(statusmessage);
    data++; // padding
    VALUE_MEMBER(statusmessage_length);
    VALUE_MEMBER(userstatus);
    data += 3; // padding
    VALUE_MEMBER(friendrequest_nospam);
    VALUE_MEMBER(last_seen_time);

#undef VALUE_MEMBER
#undef ARRAY_MEMBER

    return data;
}

static uint32_t friends_list_save(const Mess *m, uint8_t *data)
{
    uint32_t i;
    uint32_t num = 0;
    uint8_t *cur_data = data;

    for (i = 0; i < m->numfriends; i++) {
        if (friend_is_slaimer(m, i))
          continue;
        if (m->friendlist[i].status > 0) {
            struct SAVED_FRIEND temp = { 0 };
            temp.status = m->friendlist[i].status;
            memcpy(temp.real_pk, m->friendlist[i].real_pk, CRYPTO_PUBLIC_KEY_SIZE);

            if (temp.status < 3) {
                const size_t friendrequest_length =
                    MIN(m->friendlist[i].info_size,
                        MIN(SAVED_FRIEND_REQUEST_SIZE, MAX_FRIEND_REQUEST_DATA_SIZE));
                memcpy(temp.info, m->friendlist[i].info, friendrequest_length);

                temp.info_size = htons(m->friendlist[i].info_size);
                temp.friendrequest_nospam = m->friendlist[i].friendrequest_nospam;
            } else {
                memcpy(temp.name, m->friendlist[i].name, m->friendlist[i].name_length);
                temp.name_length = htons(m->friendlist[i].name_length);
                memcpy(temp.statusmessage, m->friendlist[i].statusmessage, m->friendlist[i].statusmessage_length);
                temp.statusmessage_length = htons(m->friendlist[i].statusmessage_length);
                temp.userstatus = m->friendlist[i].userstatus;

                uint8_t last_seen_time[sizeof(uint64_t)];
                memcpy(last_seen_time, &m->friendlist[i].last_seen_time, sizeof(uint64_t));
                host_to_net(last_seen_time, sizeof(uint64_t));
                memcpy(&temp.last_seen_time, last_seen_time, sizeof(uint64_t));
            }

            uint8_t *next_data = friend_save(&temp, cur_data);
            assert(next_data - cur_data == friend_size());
#ifdef __LP64__
            assert(memcmp(cur_data, &temp, friend_size()) == 0);
#endif
            cur_data = next_data;
            num++;
        }
    }

    assert(cur_data - data == num * friend_size());
    return cur_data - data;
}

static const uint8_t *friend_load(struct SAVED_FRIEND *temp, const uint8_t *data)
{
#define VALUE_MEMBER(NAME)                            \
    memcpy(&temp->NAME, data, sizeof(temp->NAME));  \
    data += sizeof(temp->NAME)

#define ARRAY_MEMBER(NAME)                            \
    memcpy(temp->NAME, data, sizeof(temp->NAME));   \
    data += sizeof(temp->NAME)

    // Exactly the same in friend_load, friend_save, and friend_size
    VALUE_MEMBER(status);
    ARRAY_MEMBER(real_pk);
    ARRAY_MEMBER(info);
    data++; // padding
    VALUE_MEMBER(info_size);
    ARRAY_MEMBER(name);
    VALUE_MEMBER(name_length);
    ARRAY_MEMBER(statusmessage);
    data++; // padding
    VALUE_MEMBER(statusmessage_length);
    VALUE_MEMBER(userstatus);
    data += 3; // padding
    VALUE_MEMBER(friendrequest_nospam);
    VALUE_MEMBER(last_seen_time);

#undef VALUE_MEMBER
#undef ARRAY_MEMBER

    return data;
}

static int friends_list_load(Mess *m, const uint8_t *data, uint32_t length)
{
    if (length % friend_size() != 0) {
        return -1;
    }

    uint32_t num = length / friend_size();
    uint32_t i;
    const uint8_t *cur_data = data;

    for (i = 0; i < num; ++i) {
        struct SAVED_FRIEND temp = { 0 };
        const uint8_t *next_data = friend_load(&temp, cur_data);
        assert(next_data - cur_data == friend_size());
#ifdef __LP64__
        assert(memcmp(&temp, cur_data, friend_size()) == 0);
#endif
        cur_data = next_data;

        if (temp.status >= 3) {
            int fnum = m_addfriend_norequest(m, temp.real_pk);

            if (fnum < 0) {
                continue;
            }

            setfriendname(m, fnum, temp.name, ntohs(temp.name_length));
            set_friend_statusmessage(m, fnum, temp.statusmessage, ntohs(temp.statusmessage_length));
            set_friend_userstatus(m, fnum, temp.userstatus);
            uint8_t last_seen_time[sizeof(uint64_t)];
            memcpy(last_seen_time, &temp.last_seen_time, sizeof(uint64_t));
            net_to_host(last_seen_time, sizeof(uint64_t));
            memcpy(&m->friendlist[fnum].last_seen_time, last_seen_time, sizeof(uint64_t));
        } else if (temp.status != 0) {
            /* TODO(irungentoo): This is not a good way to do this. */
            uint8_t address[FRIEND_ADDRESS_SIZE];
            id_copy(address, temp.real_pk);
            memcpy(address + CRYPTO_PUBLIC_KEY_SIZE, &(temp.friendrequest_nospam), sizeof(uint32_t));
            uint16_t checksum = address_checksum(address, FRIEND_ADDRESS_SIZE - sizeof(checksum));
            memcpy(address + CRYPTO_PUBLIC_KEY_SIZE + sizeof(uint32_t), &checksum, sizeof(checksum));
            m_addfriend(m, address, temp.info, ntohs(temp.info_size));
        }
    }

    return num;
}

/*  return size of the messenger data (for saving) */

uint32_t messenger_size(const Mess *m)
{
    uint32_t size32 = sizeof(uint32_t), sizesubhead = size32 * 2;
    uint32_t s=0;
    if ((m->savedata.data_length > 0)  &&
          (bool_array_all(m->savedata.data_received, true, m->savedata.data_length)==true)) {
      s += m->savedata.data_length;
    }
    s += size32 * 2; // global cookie for antitaxi
    //                    server pk               pass host  port
    s += sizesubhead + CRYPTO_PUBLIC_KEY_SIZE + 21 + 21 + size32+size32;
    s += sizesubhead; // end cookie
    return s;
}

static uint8_t *z_state_save_subheader(uint8_t *data, uint32_t len, uint16_t type)
{
    host_to_lendian32(data, len);
    data += sizeof(uint32_t);
    host_to_lendian32(data, (host_tolendian16(MESSENGER_STATE_COOKIE_TYPE) << 16) | host_tolendian16(type));
    data += sizeof(uint32_t);
    return data;
}

/* Save the messenger in data of size Messenger_size(). */

void messenger_save(const Mess *m, uint8_t *data)
{
    memset(data, 0, messenger_size(m));

    uint32_t len;
    uint16_t type;
    uint32_t size32 = sizeof(uint32_t);
    
    
    
    if ((m->savedata.data_length > 0)  && 
          bool_array_all(m->savedata.data_received, true, m->savedata.data_length)) {
      memcpy(data, m->savedata.data, m->savedata.data_length);  ; // + TOT SAVE DATA ANTITOX
      data += m->savedata.data_length;
    }
    // global cookie
    memset(data, 0, size32);
    data += size32;
    host_to_lendian32(data, MESSENGER_STATE_COOKIE_GLOBAL);
    data += size32;
    // antitaxi data
    len = CRYPTO_PUBLIC_KEY_SIZE + 21 + 21 + size32+size32;
    type = MESSENGER_STATE_TYPE_ANTITAXI;
    data = z_state_save_subheader(data, len, type);
    memcpy(data, m->net->sessions->otherpk, CRYPTO_PUBLIC_KEY_SIZE);
    data += CRYPTO_PUBLIC_KEY_SIZE;
    memcpy(data, m->net->password, 21);
    data += 21;
    memcpy(data, m->net->host, 21);
    data += 21;
    *(uint32_t *)data = (uint32_t) m->net->port;
    data += size32;
    *(uint32_t *)data = (uint32_t) m->net->last_session_id;
    data += size32;    
    // end cookie
    z_state_save_subheader(data, 0, MESSENGER_STATE_TYPE_END);
}

static int messenger_load_state_callback(void *outer, const uint8_t *data, uint32_t length, uint16_t type)
{
    Mess *m = (Mess *)outer;
    LOGGER_DEBUG(m->log, "type: %d!", type);
    switch (type) {
        case MESSENGER_STATE_TYPE_NOSPAMKEYS:
            if (length == CRYPTO_PUBLIC_KEY_SIZE + CRYPTO_SECRET_KEY_SIZE + sizeof(uint32_t)) {
                set_nospam(&(m->fr), *(const uint32_t *)data);
                //load_secret_key(m->net_crypto, (&data[sizeof(uint32_t)]) + CRYPTO_PUBLIC_KEY_SIZE);
                set_public_key(m, (&data[sizeof(uint32_t)]));
                //if (public_key_cmp((&data[sizeof(uint32_t)]), m->self_public_key) != 0) {
                //    return -1;
                //}
            } else {
                return -1;    // critical 
            }

            break;

        case MESSENGER_STATE_TYPE_DHT:
            //DHT_load(m->dht, data, length);
            break;

        case MESSENGER_STATE_TYPE_FRIENDS:
            friends_list_load(m, data, length);
            break;

        case MESSENGER_STATE_TYPE_NAME:
            if ((length > 0) && (length <= MAX_NAME_LENGTH)) {
                setname(m, data, length);
            }

            break;

        case MESSENGER_STATE_TYPE_STATUSMESSAGE:
            if ((length > 0) && (length <= MAX_STATUSMESSAGE_LENGTH)) {
                m_set_statusmessage(m, data, length);
            }

            break;

        case MESSENGER_STATE_TYPE_STATUS:
            if (length == 1) {
                m_set_userstatus(m, *data);
            }

            break;

        case MESSENGER_STATE_TYPE_TCP_RELAY: {
            if (length == 0) {
                break;
            }

            //unpack_nodes(m->loaded_relays, NUM_SAVED_TCP_RELAYS, 0, data, length, 1);
            //m->has_added_relays = 0;

            break;
        }

        case MESSENGER_STATE_TYPE_PATH_NODE: {
            //Node_format nodes[NUM_SAVED_PATH_NODES];
            //
            //if (length == 0) {
            //    break;
            //}
            //
            //int i, num = unpack_nodes(nodes, NUM_SAVED_PATH_NODES, 0, data, length, 0);
            //
            //for (i = 0; i < num; ++i) {
            //    onion_add_bs_path_node(m->onion_c, nodes[i].ip_port, nodes[i].public_key);
            //}

            break;
        }
        case MESSENGER_STATE_TYPE_ANTITAXI: {
          
          if (length == CRYPTO_PUBLIC_KEY_SIZE + 21 + 21 + sizeof(uint32_t)+ sizeof(uint32_t)) {
            LOGGER_DEBUG(m->log, "Loading ANTITAXI data!");
            memcpy(m->net->sessions->otherpk, data, CRYPTO_PUBLIC_KEY_SIZE);
            memcpy(m->net->password, data + CRYPTO_PUBLIC_KEY_SIZE, 21);
            m->net->password[20]='\0';
            memcpy(m->net->host, data + CRYPTO_PUBLIC_KEY_SIZE + 21, 21);
            m->net->host[20]='\0';
            uint32_t port;
            lendian_to_host32(&port, data + CRYPTO_PUBLIC_KEY_SIZE + 21 + 21);
            m->net->port=(uint16_t) port;
            uint32_t last_sess_id;
            lendian_to_host32(&last_sess_id, data + CRYPTO_PUBLIC_KEY_SIZE + 21 + 21 + 4);
            m->net->last_session_id=(uint16_t) last_sess_id;
            LOGGER_DEBUG(m->log, "Loaded info: %s %s %d %d", m->net->password, m->net->host, m->net->port, m->net->last_session_id);
          } else {
            LOGGER_DEBUG(m->log, "Invalid ANTITAXI size of %d bytes!", length);
          }
          break;
        }

        case MESSENGER_STATE_TYPE_END: {
            if (length != 0) {
                return -1;
            }

            return -2;
        }

        default:
            LOGGER_ERROR(m->log, "Load state: contains unrecognized part (len %u, type %u)\n",
                         length, type);
            break;
    }

    return 0;
}

/* Load the messenger from data of size length. */
int messenger_load(Mess *m, const uint8_t *data, uint32_t length)
{
    // check cookie
    uint32_t data32[2];
    uint32_t cookie_len = sizeof(data32);
    
    

    if (length < cookie_len) {
        LOGGER_DEBUG(m->log, "FAILED!");
        return -1;
    }

    memcpy(data32, data, sizeof(uint32_t));
    lendian_to_host32(data32 + 1, data + sizeof(uint32_t));

    if (!data32[0] && (data32[1] == MESSENGER_STATE_COOKIE_GLOBAL)) {
        return load_state(messenger_load_state_callback, m->log, m, data + cookie_len,
                          length - cookie_len, MESSENGER_STATE_COOKIE_TYPE);

    } else {
      LOGGER_DEBUG(m->log, "FAILED2: %d %d!",data32[0] ,data32[1]);
    }

    return -1;
}


/* Return the number of friends in the instance m.
 * You should use this to determine how much memory to allocate
 * for copy_friendlist. */
uint32_t count_friendlist(const Mess *m)
{
    uint32_t ret = 0;
    uint32_t i;

    for (i = 0; i < m->numfriends; i++) {
        if (m->friendlist[i].status > 0) {
            ret++;
        }
    }

    return ret;
}

/* Copy a list of valid friend IDs into the array out_list.
 * If out_list is NULL, returns 0.
 * Otherwise, returns the number of elements copied.
 * If the array was too small, the contents
 * of out_list will be truncated to list_size. */
uint32_t copy_friendlist(Mess const *m, uint32_t *out_list, uint32_t list_size)
{
    if (!out_list) {
        return 0;
    }

    if (m->numfriends == 0) {
        return 0;
    }

    uint32_t i;
    uint32_t ret = 0;

    for (i = 0; i < m->numfriends; i++) {
        if (ret >= list_size) {
            break; /* Abandon ship */
        }

        if (m->friendlist[i].status > 0) {
            out_list[ret] = i;
            ret++;
        }
    }

    return ret;
}
