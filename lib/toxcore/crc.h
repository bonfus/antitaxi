#ifndef _CRC_H
#define _CRC_H

#include <inttypes.h>

#if defined __cplusplus
extern "C" {
#endif

    
uint8_t crc8_check(uint8_t *data, uint16_t len, uint8_t check_sum);
uint8_t crc8_calc(uint8_t *data, uint16_t len);

#if defined __cplusplus
}
#endif
		
#endif // _CRC_H
