#include "net.h"
#include "utlist.h"
#include "msg_sender.h"
#include "interpreter.h"
#include "crc.h"
#include "mess.h"
//#include <cstdlib>




void onDisconnect(dyad_Event *e) {
  printf("Disconnected: %s\n", e->msg);
  Mess * m = (Mess*) e->udata;
}

void onConnect(dyad_Event *e) {
  printf("CONNECTED: %s !!!!\n", e->msg);
  
  Mess * m = (Mess*) e->udata;
  m->net->connection_status = CONNECTED;
  
  if (m->net->sessions->id == UINT16_MAX) {
    // uninitialized session, ask for login
    
    msg_t * msg = new_empty_message(ANTITAXI_IS_ALIVE_ID, true, 
                                    PRTCL_UINT8STAR, 
                                    strlen(m->net->password)+1,
                                    PRTCL_UINT16,
                                    NO_MORE_FIELDS);
    INIT_WRITE_MSG(msg);
    WRITE_UINT8STAR(((uint8_t * )m->net->password),strlen(m->net->password)+1,msg);
    WRITE_UINT16(m->net->last_session_id, msg);
    msg_ref_t * msgr = new_message_ref(msg);

    LL_PREPEND(m->net->sessions->msgs, msgr);
  
  }
  
  send_queued_msgs_to_session(e->stream, m->net->sessions);
}

void onError(dyad_Event *e) {
  printf("error: %s (host %s port %d)\n", e->msg, dyad_getAddress(e->stream), dyad_getPort(e->stream));
  Mess * m = (Mess*) e->udata;
  m->net->connection_status = NOT_CONNECTED;
}

void onData(dyad_Event *e) {
  //printf("%s", e->data);
  Mess * m = (Mess*) e->udata;
  
  if (!m->net->sessions) {
    LOGGER_DEBUG(m->log, "Sessions not allocated!!! Damn bad! Will probably fail!");
    return;
  }
  
  size_t data_read=0;
  
  while(data_read<e->size) {
    
    pkt_t pkt;
    pkt.crypt_data = NULL; //by ref! no copy!
    
    data_read += buf_push_pull(&(m->net->buf), &pkt, data_read, e->data, e->size);
    
    
    if (pkt.crypt_data_len == 0) {
      continue;
    }
    
    unsigned char c_pk[crypto_box_PUBLICKEYBYTES];
    
    
    msg_t * msg = pkt2msg(m->net->sessions->mypk, m->net->sessions->mysk, pkt, c_pk);
        
    if (msg==NULL) {
      LOGGER_DEBUG(m->log, "Invalid message!!");
      return;
    }
    
    // only first session is present in clients
    if (m->net->sessions->id == UINT16_MAX && msg->type == ANTITAXI_IS_ALIVE_ID) {
      // session is not confirmed yet
    } else if (msg->sess_id != m->net->sessions->id) {
      LOGGER_DEBUG(m->log, "DIFFERENT sess ID for message type %d , %d %d!\n", msg->type, msg->sess_id , m->net->sessions->id);
      exit(1);
    }
    
    if (msg->is_reply) {
      // try to find original message
      //msg_t * orig = NULL;
      msg_ref_t * el;
      bool found = false;
      
      // only search for messages in first session
      LL_FOREACH(m->net->sessions->msgs, el) {
        if (msg->id == el->msg->id) {
          // sanity check
          if (el->msg->wants_reply == false) {
            LOGGER_DEBUG(m->log, "PANIC! This message doesn't seek replies");
            break;
          }
          // ref is not incremented in this case
          el->msg->reply = msg;
          found = true;
          break;
        }
      }
      
      if (!found) {
        // TODO ERROR!!
        LOGGER_DEBUG(m->log,"Reply got lost! type: %d, msg id: %d, sess num: %d", msg->type, msg->id, msg->sess_id);
        free_msg(msg);
      }
      
    } else { // is a callback, and callbacks never wants replies
      LOGGER_DEBUG(m->log, "Appendig callback type %d", msg->type);
      msg_ref_t * msgr = new_callback_ref(msg);
      LL_APPEND(m->net->sessions->msgs, msgr);
    }
  }
  
}


