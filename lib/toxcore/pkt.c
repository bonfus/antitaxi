#include <sodium.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#include "pkt.h"


msg_t * pkt2msg( unsigned char self_pk[], unsigned char self_sk[], 
              pkt_t pkt, unsigned char * pub_key)
{

  uint8_t reqid;
  uint8_t data[MAX_CRYPTO_REQUEST_SIZE];
    
  int data_len = handle_request(self_pk, self_sk, pub_key, data, &reqid, pkt.crypt_data, pkt.crypt_data_len);
  //check reqid
  if (data_len > 0) {
    return data2msg(data, data_len);
  } else {
    return NULL;
  }
}

bool msg2pkt(session_t * sess, msg_t * msg, pkt_t * pkt)
{
  
  write_msg_header(sess->id,msg);
  
  
  int ret = create_request(sess->mypk, 
                                      sess->mysk, 
                                      pkt->crypt_data, 
                                      sess->otherpk, 
                                      msg->data, 
                                      msg->data_len, 
                                      PKTID);
  if (ret>0) {
    pkt->crypt_data_len = ret;
    return true;
  }
  else {
    pkt->crypt_data_len = 0;
    return false;
  }
}

