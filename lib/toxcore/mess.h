#ifndef _MESS_H
#define _MESS_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdint.h>
#include <sodium.h>
#include "endian.h"
#include "tox.h"
#include "tox_defs.h"
#include "network_defs.h"
#include "logger.h"
#include "net.h"
#include "util.h"
#include "friend_requests.h"
#include "ccompat.h"

//#define TOX_MAX_NAME_LENGTH MAX_NAME_LENGTH
//#define TOX_MAX_STATUS_MESSAGE_LENGTH MAX_STATUSMESSAGE_LENGTH

#define FILE_ID_LENGTH 32

//#define CRYPTO_PUBLIC_KEY_SIZE crypto_box_PUBLICKEYBYTES

enum {
    MESSAGE_NORMAL,
    MESSAGE_ACTION
};

/* new messenger format for load/save, more robust and forward compatible */

// global cookie + ANTITX cookie + PK + pass + host + port + last_session_id + end cookie
#define ANTITAXI_TOTAL_SAVE_DATA_SIZE 8+8+32+21+21+4+4+8

#define MESSENGER_STATE_COOKIE_GLOBAL 0x15ed1b1f

#define MESSENGER_STATE_COOKIE_TYPE      0x01ce
#define MESSENGER_STATE_TYPE_NOSPAMKEYS    1
#define MESSENGER_STATE_TYPE_DHT           2
#define MESSENGER_STATE_TYPE_FRIENDS       3
#define MESSENGER_STATE_TYPE_NAME          4
#define MESSENGER_STATE_TYPE_STATUSMESSAGE 5
#define MESSENGER_STATE_TYPE_STATUS        6
#define MESSENGER_STATE_TYPE_TCP_RELAY     10
#define MESSENGER_STATE_TYPE_PATH_NODE     11
#define MESSENGER_STATE_TYPE_END           255
#define MESSENGER_STATE_TYPE_ANTITAXI      254

#define SAVED_FRIEND_REQUEST_SIZE 1024
#define NUM_SAVED_PATH_NODES 8

/* Default start timeout in seconds between friend requests. */
#define FRIENDREQUEST_TIMEOUT 5;

enum {
    CONNECTION_NONE,
    CONNECTION_TCP,
    CONNECTION_UDP,
    CONNECTION_UNKNOWN
};



typedef struct {
    uint8_t ipv6enabled;
    uint8_t udp_disabled;
    TCP_Proxy_Info proxy_info;
    uint16_t port_range[2];
    uint16_t tcp_server_port;

    uint8_t hole_punching_enabled;
    bool local_discovery_enabled;

    logger_cb *log_callback;
    void *log_user_data;
} Mess_Options;


struct Receipts {
    uint32_t packet_num;
    uint32_t msg_id;
    struct Receipts *next;
};

/* Status definitions. */
enum {
    NOFRIEND,
    FRIEND_ADDED,
    FRIEND_REQUESTED,
    FRIEND_CONFIRMED,
    FRIEND_ONLINE,
    FRIEND_SLAIMER //= -5
};

/* Errors for m_addfriend
 * FAERR - Friend Add Error
 */
enum {
    FAERR_TOOLONG = -1,
    FAERR_NOMESSAGE = -2,
    FAERR_OWNKEY = -3,
    FAERR_ALREADYSENT = -4,
    FAERR_BADCHECKSUM = -6,
    FAERR_SETNEWNOSPAM = -7,
    FAERR_NOMEM = -8
};

typedef enum {
    USERSTATUS_NONE,
    USERSTATUS_AWAY,
    USERSTATUS_BUSY,
    USERSTATUS_INVALID
}
USERSTATUS;



typedef struct Mess Mess;

typedef struct Friend_stuct {
    uint8_t real_pk[crypto_box_PUBLICKEYBYTES];
    uint8_t self_public_key[crypto_box_PUBLICKEYBYTES];
    uint8_t friendcon;  // now contains UDP, TCP or NONE
    uint64_t friendrequest_lastsent; // Time at which the last friend request was sent.
    uint32_t friendrequest_timeout; // The timeout between successful friendrequest sending attempts.
    uint8_t status; // 0 if no friend, 1 if added, 2 if friend request sent, 3 if confirmed friend, 4 if online.
    uint8_t info[MAX_FRIEND_REQUEST_DATA_SIZE]; // the data that is sent during the friend requests we do.
    uint8_t name[MAX_NAME_LENGTH];
    uint16_t name_length;
    uint8_t name_sent; // 0 if we didn't send our name to this friend 1 if we have.
    uint8_t statusmessage[MAX_STATUSMESSAGE_LENGTH];
    uint16_t statusmessage_length;
    uint8_t statusmessage_sent;
    USERSTATUS userstatus;
    uint8_t userstatus_sent;
    uint8_t user_istyping;
    uint8_t user_istyping_sent;
    uint8_t is_typing;
    uint16_t info_size; // Length of the info.
    uint32_t message_id; // a semi-unique id used in read receipts.
    uint32_t friendrequest_nospam; // The nospam number used in the friend request.
    uint64_t last_seen_time;
    uint8_t last_connection_udp_tcp;
/*
    struct File_Transfers file_sending[MAX_CONCURRENT_FILE_PIPES];
    unsigned int num_sending_files;
    struct File_Transfers file_receiving[MAX_CONCURRENT_FILE_PIPES];

    struct {
        int (*function)(Messenger *m, uint32_t friendnumber, const uint8_t *data, uint16_t len, void *object);
        void *object;
    } lossy_rtp_packethandlers[PACKET_LOSSY_AV_RESERVED];
*/
    struct Receipts *receipts_start;
    struct Receipts *receipts_end;
 
    struct Friend_stuct * prev;
    struct Friend_stuct * next;
} Friend;

typedef struct savedata_struct {
  uint8_t * data;
  bool * data_received;
  size_t data_length;
} savedata_t;

typedef struct slaimer_msgs_struct {
  char msg[51];
  struct slaimer_msgs_struct * next;
} slaimer_msgs_t;

typedef struct antitaxi_read_rec_struct {
  
  bool server_received;
  bool server_is_keeping;
  bool friend_received;
  uint32_t at_msg_id;
  int32_t friendnumber;
  struct antitaxi_read_rec_struct * next;
} antitaxi_read_rec_t;

struct Mess{

  Logger *log;
  _net * net;
  savedata_t savedata;
  slaimer_msgs_t * slaimer_log;
  antitaxi_read_rec_t * at_read_rec;
  
  void (*friend_req)(struct Friend_Requests* fr, const uint8_t *,const uint8_t *,uint16_t,void *);
  // new functions above
  
  uint8_t self_public_key[crypto_box_PUBLICKEYBYTES];

  uint8_t name[TOX_MAX_NAME_LENGTH];
  uint16_t name_length;

  uint8_t statusmessage[MAX_STATUSMESSAGE_LENGTH];
  uint16_t statusmessage_length;

  USERSTATUS userstatus;

  Friend *friendlist;
  uint32_t numfriends;

  void (*friend_message)(struct Mess *m, uint32_t, unsigned int, const uint8_t *, size_t, void *);
  void (*friend_namechange)(struct Mess *m, uint32_t, const uint8_t *, size_t, void *);
  void (*friend_statusmessagechange)(struct Mess *m, uint32_t, const uint8_t *, size_t, void *);
  void (*friend_userstatuschange)(struct Mess *m, uint32_t, unsigned int, void *);
  void (*friend_typingchange)(struct Mess *m, uint32_t, bool, void *);
  void (*read_receipt)(struct Mess *m, uint32_t, uint32_t, void *);
  void (*friend_connectionstatuschange)(struct Mess *m, uint32_t, unsigned int, void *);
  void (*friend_connectionstatuschange_internal)(struct Mess *m, uint32_t, uint8_t, void *);
  void *friend_connectionstatuschange_internal_userdata;

  void *conferences_object; /* Set by new_groupchats()*/
  void (*conference_invite)(struct Mess *m, uint32_t, const uint8_t *, uint16_t, void *);

  void (*file_sendrequest)(struct Mess *m, uint32_t, uint32_t, uint32_t, uint64_t, const uint8_t *, size_t,
                            void *);
  void (*file_filecontrol)(struct Mess *m, uint32_t, uint32_t, unsigned int, void *);
  void (*file_filedata)(struct Mess *m, uint32_t, uint32_t, uint64_t, const uint8_t *, size_t, void *);
  void (*file_reqchunk)(struct Mess *m, uint32_t, uint32_t, uint64_t, size_t, void *);

  void (*msi_packet)(struct Mess *m, uint32_t, const uint8_t *, uint16_t, void *);
  void *msi_packet_userdata;

  void (*lossy_packethandler)(struct Mess *m, uint32_t, const uint8_t *, size_t, void *);
  void (*lossless_packethandler)(struct Mess *m, uint32_t, const uint8_t *, size_t, void *);

  void (*core_connection_change)(struct Mess *m, unsigned int, void *);
  unsigned int last_connection_status;

  Mess_Options options;
  Friend_Requests fr;
};


enum {
    MESSENGER_ERROR_NONE,
    MESSENGER_ERROR_PORT,
    MESSENGER_ERROR_TCP_SERVER,
    MESSENGER_ERROR_OTHER
};

void init_net(Mess *m);
void iterate_connection(Mess *m, double timeout);
void shutdown_net(Mess *m);
int32_t getfriend_id(const Mess *m, const uint8_t *real_pk);
int get_real_pk(const Mess *m, int32_t friendnumber, uint8_t *real_pk);
int getfriendcon_id(const Mess *m, int32_t friendnumber);
void getaddress(const Mess *m, uint8_t *address);
int32_t m_addfriend(Mess *m, const uint8_t *address, const uint8_t *data, uint16_t length);
int32_t m_addfriend_norequest(Mess *m, const uint8_t *real_pk);
int m_delfriend(Mess *m, int32_t friendnumber);
int m_set_friend_connectionstatus(Mess *m, int32_t friendnumber, uint8_t cstat, void *userdata);
int m_get_friend_connectionstatus(const Mess *m, int32_t friendnumber);
int m_query_friends_connection(const Mess *m);
int m_friend_exists(const Mess *m, int32_t friendnumber);
int m_send_message_generic(Mess *m, int32_t friendnumber, uint8_t type, const uint8_t *message, uint32_t length, uint32_t *message_id);
int setfriendname(Mess *m, int32_t friendnumber, const uint8_t *name, uint16_t length);
int m_set_friendname(Mess *m, int32_t friendnumber, const uint8_t *name, uint16_t length, void *userdata);
int setname(Mess *m, const uint8_t *name, uint16_t length);
uint16_t getself_name(const Mess *m, uint8_t *name);
int getname(const Mess *m, int32_t friendnumber, uint8_t *name);
int m_get_name_size(const Mess *m, int32_t friendnumber);
int m_get_self_name_size(const Mess *m);
int m_set_statusmessage(Mess *m, const uint8_t *status, uint16_t length);
int m_set_userstatus(Mess *m, uint8_t status);
int m_get_statusmessage_size(const Mess *m, int32_t friendnumber);
int m_copy_statusmessage(const Mess *m, int32_t friendnumber, uint8_t *buf, uint32_t maxlen);
int m_get_self_statusmessage_size(const Mess *m);
int m_copy_self_statusmessage(const Mess *m, uint8_t *buf);
uint8_t m_get_userstatus(const Mess *m, int32_t friendnumber);
uint8_t m_get_self_userstatus(const Mess *m);
uint64_t m_get_last_online(const Mess *m, int32_t friendnumber);
int m_set_usertyping(Mess *m, int32_t friendnumber, uint8_t is_typing);
int m_get_istyping(const Mess *m, int32_t friendnumber);
int m_set_friend_statusmessage(Mess *m, int32_t friendnumber, const uint8_t *status, uint16_t length, void *userdata);
void m_set_friend_userstatus(Mess *m, int32_t friendnumber, uint8_t status, void *userdata);
void m_set_friend_typing(Mess *m, int32_t friendnumber, uint8_t is_typing, void * userdata);
void m_callback_log(Mess *m, logger_cb *function, void *context, void *userdata);
void m_callback_friendrequest(Mess *m, void (*function)(Mess *m, const uint8_t *, const uint8_t *, size_t, void *));
void m_callback_friendmessage(Mess *m, void (*function)(Mess *m, uint32_t, unsigned int, const uint8_t *, size_t, void *));
void m_callback_namechange(Mess *m, void (*function)(Mess *m, uint32_t, const uint8_t *, size_t, void *));
void m_callback_statusmessage(Mess *m, void (*function)(Mess *m, uint32_t, const uint8_t *, size_t, void *));
void m_callback_userstatus(Mess *m, void (*function)(Mess *m, uint32_t, unsigned int, void *));
void m_callback_typingchange(Mess *m, void (*function)(Mess *m, uint32_t, _Bool, void *));
void m_callback_read_receipt(Mess *m, void (*function)(Mess *m, uint32_t, uint32_t, void *));
void m_callback_connectionstatus(Mess *m, void (*function)(Mess *m, uint32_t, unsigned int, void *));
void m_callback_core_connection(Mess *m, void (*function)(Mess *m, unsigned int, void *));
bool grab_remote_options(Mess *m);
bool fill_remote_options(Mess *m, const uint8_t * data, size_t length);
void set_public_key(Mess *m, const uint8_t *pk);
void m_set_friend_message(Mess *m, int32_t friendnumber, TOX_MESSAGE_TYPE type, uint8_t *message, size_t length, void *userdata);
void m_set_friend_read_receipt(Mess *m, int32_t friendnumber, uint32_t msg_id);
void m_set_server_read_receipt(Mess *m, int32_t friendnumber, uint32_t msg_id, bool is_keeping);
void m_callback_msi_packet(Mess *m, void (*function)(Mess *m, uint32_t, const uint8_t *, uint16_t, void *), void *userdata);
int m_msi_packet(const Mess *m, int32_t friendnumber, const uint8_t *data, uint16_t length);
void m_callback_connectionstatus_internal_av(Mess *m, void (*function)(Mess *m, uint32_t, uint8_t, void *), void *userdata);
void connection_status_cb(Mess *m, uint8_t conn_status, void *userdata);
Mess *new_mess(Mess_Options *options, unsigned int *error);
void kill_mess(Mess *m);
void m_done_bootstrap(const Mess *m);
void do_mess(Mess *m, void *user_data);
uint32_t messenger_size(const Mess *m);
void messenger_save(const Mess *m, uint8_t *data);
int messenger_load(Mess *m, const uint8_t *dat, uint32_t len);
uint32_t count_friendlist(const Mess *m);
uint32_t copy_friendlist(Mess const *m, uint32_t *out_list, uint32_t list_size);
#endif
