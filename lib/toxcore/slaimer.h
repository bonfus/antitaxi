#ifndef _SLAIMER_H
#define _SLAIMER_H

#include "mess.h"

bool slaimer_message(Mess * m, const uint8_t * msg, size_t len);
#endif
